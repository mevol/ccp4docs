REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac version 5.\*
-------------------------------------------------

DICTIONARY
==========

Links to non-commercial editors to derive coordinates from chemical description
-------------------------------------------------------------------------------

`CORINA <http://www2.ccc.uni-erlangen.de/services/3d.html>`__
    This site gives very good coordinates if you supply
    `SMILES <http://www.daylight.com/dayhtml/smiles/>`__ strings. It can
    recognise simple or stereo SMILES strings. The best way of accessing
    this site is to draw a molecule using
    `CACTVS <http://www2.ccc.uni-erlangen.de/software/cactvs/tools.html>`__
    and using SMILES derived from CACTVS to send to CORINA to derive
    coordinates.
`Monosaccharide
Database <http://www.cermav.cnrs.fr/databank/mono/index2.html>`__
    It contains various monosaccharides. They could be taken in a PDB
    format and used as a building block for ligands containing sugars.
    PDB file may have to be edited to replace charges with B-values.
`Disaccharide
Database <http://www.cermav.cnrs.fr/databank/disacch/index.html>`__
    It contains various disaccharides. Various conformations can be
    chosen using pictures provided. Output will be PDB file. This PDB
    file can be used to create a ligand description or as building
    blocks for the ligand under investigation
`Enhanced NCI Database
Browser <http://www2.ccc.uni-erlangen.de/ncidb/frame.html>`__
    Using this site one can search for structure with a given
    substructure.
`Sweet2 - Sweet
II <http://www.dkfz-heidelberg.de/spec/sweet2/doc/index.html>`__
    This site can give you polysaccharides.

    **NOTE:** In some cases atom names may not be as you wish them to
    be. Before running REFMAC or LIBCHECK to create a dictionary, it is
    better to rename atoms to your or IUPAC or somebody's satisfaction.
