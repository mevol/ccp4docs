REFMAC (CCP4: Supported Program)
================================

NAME
----

**refmac version 5.0.32** - macromolecular refinement program

CREDITS
-------

AUTHORS
-------

`Garib Murshudov <http://www.ysbl.york.ac.uk/~garib>`__, University of
York and CLRC, Daresbury Laboratory.

and

`Alexei Vagin <http://www.ysbl.york.ac.uk/~alexei>`__, `Martyn
Winn <http://www.dl.ac.uk/CCP/CCP4/martyn/martyn.html>`__, `Roberto
Steiner <mailto:R.A.Steiner@chem.rug.nl>`__, `Eleanor
Dodson <http://www.yorvic.york.ac.uk/people/6.htm>`__, `Maria
Turkenburg <http://www.ysbl.york.ac.uk/~mgwt>`__, `Kim
Henrick <http://www.ebi.ac.uk/~henrick>`__, `Liz
Potterton <http://www.ysbl.york.ac.uk/~lizp>`__

Special thanks to: I should add. Many, many people.

| Phil Evans - for testing and very, very, very useful comments and
  suggestions
| Misha Isupov
| Tasos Perrakis

This work has been and continues being supported by CCP4 and BBSRC grant
for CCP4
