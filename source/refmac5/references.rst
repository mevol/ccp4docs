REFMAC (CCP4: Supported Program)
================================

NAME
----

**refmac version 5.5.0051** - macromolecular refinement program

REFERENCES
----------

***If you use REFMAC please refer to one of these papers!!!***

#. "Application of Maximum Likelihood Refinement" G. Murshudov, A.Vagin
   and E.Dodson, (1996) in the Refinement of Protein structures,
   Proceedings of Daresbury Study Weekend.
#. "Refinement of Macromolecular Structures by the Maximum-Likelihood
   method" G.N. Murshudov, A.A.Vagin and E.J.Dodson, (1997) in Acta
   Cryst. **D53**, 240-255.
#. "Incorporation of Prior Phase Information Strengthen
   Maximum-Likelihood Structure Refinemen" N.J.Pannu, G.N.Murshudov,
   E.J.Dodson and R.J.ReadA (1998) Acta Cryst. section **D54**,
   1285-1294.
#. "Efficient anisotropic refinement of Macromolecular structures using
   FFT" G.N.Murshudov, A.Lebedev, A.A.Vagin, K.S.Wilson and E.J.Dodson
   (1999) Acta Cryst. section **D55**, 247-255.
#. "Use of TLS parameters to model anisotropic displacements in
   macromolecular refinement" M. Winn, M. Isupov and G.N.Murshudov
   (2000) Acta Cryst. 2001:D57 122-133
#. "Fisher's information matrix in maximum likelihood molecular
   refinement." Steiner R, Lebedev, A, Murshudov GN. Acta Cryst. 2003
   D59: 2114-2124
#. "Macromolecular TLS refinement in REFMAC at moderate resolutions,"
   Winn MD, Murshudov GN, Papiz MZ. Method in Enzymology, 2003:374
   300-321
#. "Direct incorporation of experimental phase information in model
   refinement" Skubak P, Murshudov GN, Pannu NS. Acta Cryst. 2004 D60:
   2196-2201
#. "REFMAC5 dictionary: organisation of prior chemical knowledge and
   guidelines for its use." Vagin, AA, Steiner, RS, Lebedev, AA,
   Potterton, L, McNicholas, S, Long, F and Murshudov, GN. Acta Cryst.
   2004 D60: 2284-2295
