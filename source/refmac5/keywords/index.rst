REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac\_5.\*
------------------------------------------

KEYWORDED INPUT
---------------

Anything input on a line after "!" or "#" is ignored and lines can be
continued by using a minus (-) sign. The program only checks the first 4
characters of each keyword. The order of the cards is not important
except that an END card must be last. Some keywords have various
subsidiary keywords. The available keywords in alphabetical order are:

    `**ANGLe** <restraints.html#angl>`__,
    `**BFACtor/TEMPerature** <restraints.html#bfac>`__,
    `**BINS/RANGE** <xray-general.html#bins>`__,
    `**BLIMit** <xray-general.html#blim>`__,
    `**CELL\_parameters** <xray-general.html#cell>`__,
    `**CHIRal\_volumes** <restraints.html#chir>`__,
    `**DAMPing\_factor** <xray-general.html#damp>`__,
    `**DISTance** <restraints.html#dist>`__,
    `**DNAME** <harvesting.html#dname>`__,
    `**END/GO** <xray-general.html#end>`__,
    `**FREE** <xray-general.html#free>`__,
    `**HOLD** <restraints.html#hold>`__,
    `**LABIn** <xray-principal.html#labin>`__,
    `**LABOut** <xray-general.html#labout>`__,
    `**MAKE\_restraints** <restraints.html#makecif>`__,
    `**MODE** <xray-general.html#mode>`__,
    `**MONItor** <xray-general.html#moni>`__,
    `**NCYCle** <xray-principal.html#ncyc>`__,
    `**NCSR/NONX** <restraints.html#ncsr>`__,
    `**NOHARVEST** <harvesting.html#noharvest>`__,
    `**PHASe** <xray-general.html#phas>`__,
    `**PLANe** <restraints.html#plan>`__,
    `**PNAME** <harvesting.html#pname>`__,
    `**PRIVATE** <harvesting.html#private>`__,
    `**RBONd** <restraints.html#rbon>`__,
    `**REFInement** <xray-principal.html#refi>`__,
    `**RIGId\_body** <xray-general.html#rigi>`__,
    `**RSIZE** <harvesting.html#rsize>`__,
    `**SCALe** <xray-principal.html#scal>`__,
    `**SCPArt** <xray-general.html#scpa>`__,
    `**SHANnon\_factor** <xray-general.html#shan>`__,
    `**SIGMaA** <xray-principal.html#scal_mlsc>`__,
    `**SOLVent** <xray-principal.html#solv>`__,
    `**SPHEricity** <restraints.html#sphe>`__,
    `**SYMM** <xray-general.html#symm>`__,
    `**TLSC** <xray-general.html#tlsc>`__,
    `**TORSION** <restraints.html#tors>`__,
    `**TWIN** <keywords_5_5.html#Twin>`__
    `**USECWD** <harvesting.html#usecwd>`__,
    `**VDWR/VAND** <restraints.html#vdwr>`__,
    `**WEIG** <xray-principal.html#weig>`__,

`Keywords document for 5.5.x: <keywords_5_5.html>`__
    `Labin: labels from mtz <keywords_5_5.html#Labin>`__
    `Twin refinement <keywords_5_5.html#Twin>`__
    `Simultaneous experimental phasing and
    refinement <keywords_5_5.html#sadmad>`__
    `Weighting xray and geometry terms <keywords_5_5.html#Weight>`__
    `Using segment id <keywords_5_5.html#Segid>`__
    `Harmonic restraints <keywords_5_5.html#Harmonic>`__
    `Various protocols <keywords_5_5.html#Protocols>`__
`Principal keywords controlling Xray
refinement: <xray-principal.html>`__
    `**LABI** <xray-principal.html#labin>`__ - Input MTZ labels
    `**NCYC** <xray-principal.html#ncyc>`__ - Number of the refinement
    cycles
    `**REFI** <xray-principal.html#refi>`__ - Refinement parameters
    `**SCAL** <xray-principal.html#scal>`__ - Scale parameters
    `**SIGM** <xray-principal.html#scal_mlsc>`__ - Parameters of the
    likelihood (sigmaA)
    `**SOLV** <xray-principal.html#solv>`__ - Parameters of the solvent
    `**WEIG** <xray-principal.html#weig>`__ - Weighting X-ray vs
    geometry
`All keywords for Xray refinement: <xray-general.html>`__
    `**BINS/RANGE** <xray-general.html#bins>`__ - Number of the
    resolution bins
    `**BLIM** <xray-general.html#blim>`__ - Limits of allowed B value
    range
    `**CELL** <xray-general.html#cell>`__ - Cell parameters
    `**DAMP** <xray-general.html#damp>`__ - Factors to scale down shifts
    at every cycle
    `**END/GO** <xray-general.html#end>`__ - End of keywords; must be
    last
    `**FREE** <xray-general.html#free>`__ - Flag of the reflections
    excluded from the refinement
    `**LABO** <xray-general.html#labout>`__ - Output MTZ labels. Useful
    for map calculation
    `**MODE** <xray-general.html#mode>`__ - Refinement mode
    `**MONI** <xray-general.html#moni>`__ - Level of monitoring
    statistics during refinement
    `**PHAS** <xray-general.html#phas>`__ - Parameters for the phased
    refinement
    `**RIGI** <xray-general.html#rigi>`__ - Parameters of the rigid body
    refinement
    `**SCPA** <xray-general.html#scpa>`__ - For scaling of the external
    partial structure factors
    `**SHAN** <xray-general.html#shan>`__ - Shannon factor to control
    grid spacings
    `**SYMM** <xray-general.html#symm>`__ - Symmetry
    `**TLSC** <xray-general.html#tlsc>`__ - Number of TLS cycles
`Keywords controlling (geometric) restraints (function very similar to
that in PROLSQ): <restraints.html>`__
    `**ANGL** <restraints.html#angl>`__ - Restraints on bond angles
    `**BFAC/TEMP** <restraints.html#bfac>`__ - Restraints on B values
    `**CHIR** <restraints.html#chir>`__ - Restraints on chiral volumes
    `**DIST** <restraints.html#dist>`__ - Restraints on bond distances
    `**HOLD** <restraints.html#hold>`__ - Restraints against excessive
    shifts
    `**MAKE** <restraints.html#makecif>`__ - Controls making restraints
    and checking coordinates against dictionary
    `**NCSR/NONX** <restraints.html#ncsr>`__ - Restraints on
    non-crystallographic symmetry
    `**PLAN** <restraints.html#plan>`__ - Restraints on planarity
    `**RBON** <restraints.html#rbon>`__ - Rigid bond restraints on the
    anisotropic B values of bonded atoms
    `**SPHE** <restraints.html#sphe>`__ - Sphericity restraints on the
    anisotropic B values
    `**TORS** <restraints.html#tors>`__ - Restraints on the torsion
    angles
    `**VDWR/VAND** <restraints.html#vdwr>`__ - Restraints on VDW
    repulsions
`Optional keywords controlling the data harvesting
functionality: <harvesting.html>`__
    `**DNAME** <harvesting.html#dname>`__ - Dataset name
    `**NOHARVEST** <harvesting.html#noharvest>`__ - Harvesting files are
    not necessary
    `**PNAME** <harvesting.html#pname>`__ - Project name
    `**PRIVATE** <harvesting.html#private>`__ - Controls modes of the
    files created for harvesting
    `**RSIZE** <harvesting.html#rsize>`__ - Number of characters in one
    column
    `**USECWD** <harvesting.html#usecwd>`__ - Controls directory where
    harvesting files are
