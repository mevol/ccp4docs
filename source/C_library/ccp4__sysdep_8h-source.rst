`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_sysdep.h
==============

`Go to the documentation of this file. <ccp4__sysdep_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_sysdep.h: System-dependent definitions
    00003      Copyright (C) 2001  CCLRC
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00018 #ifndef __CCP4_BITS
    00019 #define __CCP4_BITS
    00020 
    00021 #if defined (_AIX) || defined(___AIX)
    00022 #  define KNOWN_MACHINE
    00023 #  define CALL_LIKE_HPUX 1
    00024 #endif
    00025 
    00026 #if defined (__hpux) 
    00027 #  define KNOWN_MACHINE
    00028 #  define CALL_LIKE_HPUX 1
    00029 #endif
    00030 
    00031 #ifdef __sgi   /* in ANSI mode */
    00032 #  ifndef sgi
    00033 #    define sgi
    00034 #  endif
    00035 #endif
    00036 
    00037 #if defined (sgi)
    00038 #  define KNOWN_MACHINE
    00039 #  define CALL_LIKE_SUN 1
    00040 #endif
    00041 
    00042 #if defined (sun) || defined (__sun)
    00043 #  define KNOWN_MACHINE
    00044 #  define CALL_LIKE_SUN 1
    00045 #  if !defined(__STDC__) || defined(__GNUC__)
    00046 #    if !defined(G77)
    00047       extern char *sys_errlist [];
    00048 #     define strerror(i) sys_errlist[i] /* k&r compiler doesn't have it */
    00049 #    endif
    00050 #  endif
    00051 #endif
    00052 
    00053 #if defined(__OSF1__) || defined(__osf__)
    00054 #  define KNOWN_MACHINE
    00055 #  define CALL_LIKE_SUN 1
    00056 #endif
    00057 
    00058 #ifndef VMS
    00059 #  if defined (vms) || defined (__vms) || defined (__VMS)
    00060 #    define VMS
    00061 #  endif
    00062 #endif
    00063 #if defined (VMS)
    00064 #  define KNOWN_MACHINE
    00065 #  define CALL_LIKE_VMS 1
    00066 #endif
    00067 
    00068 #if defined(_MSC_VER) || defined (WIN32)
    00069 # if defined (_MSC_VER) && (_MSC_VER >= 800)
    00070 #  define CALL_LIKE_MVS 2
    00071 # else
    00072 #  define CALL_LIKE_MVS 1
    00073 # endif
    00074 #  define KNOWN_MACHINE
    00075 #endif
    00076 
    00077 #if defined (linux) || defined __linux__ || defined (__CYGWIN__)
    00078 #  undef CALL_LIKE_SUN
    00079 #  define KNOWN_MACHINE
    00080 #  define CALL_LIKE_SUN 1
    00081 #endif
    00082 
    00083 #if defined __linux__ && ( defined __PPC || defined __PPC__ )
    00084 #  undef CALL_LIKE_SUN
    00085 #  define KNOWN_MACHINE
    00086 #  define CALL_LIKE_SUN 1
    00087 #endif
    00088 
    00089 #if defined (__FreeBSD__)
    00090 #  undef CALL_LIKE_SUN
    00091 #  define KNOWN_MACHINE
    00092 #  define CALL_LIKE_SUN 1
    00093 #endif
    00094 
    00095 #if defined(F2C) || defined(G77)
    00096 #  undef CALL_LIKE_SUN
    00097 #  define CALL_LIKE_SUN 1
    00098 #  define KNOWN_MACHINE
    00099 #endif
    00100 
    00101 #if defined(__APPLE__)
    00102 #  undef CALL_LIKE_SUN
    00103 #  define CALL_LIKE_SUN 1
    00104 #  define KNOWN_MACHINE
    00105 #endif
    00106 
    00107 #if defined (_CALL_SYSV) && ! defined (__APPLE__)
    00108 #  undef CALL_LIKE_SUN
    00109 #  define CALL_LIKE_SUN 1
    00110 #  define KNOWN_MACHINE
    00111 #endif
    00112 
    00113 #if ! defined (KNOWN_MACHINE)
    00114 #  error System type is not known -- see the Installation Guide
    00115 #else
    00116 
    00117 #ifndef _POSIX_SOURCE
    00118 #define _POSIX_SOURCE
    00119 #endif
    00120 
    00121 /* include X/Open Unix extensions (e.g. cuserid) */
    00122 #ifndef _XOPEN_SOURCE
    00123 #define _XOPEN_SOURCE
    00124 #endif
    00125 
    00126 #include <stdio.h>
    00127 
    00128 #if defined (VMS)
    00129 #  include <descrip.h>          /* non-POSIX */
    00130 #  define NOUNISTD
    00131 #else
    00132 #  include <sys/types.h>
    00133 #  include <sys/stat.h>
    00134 #  if !defined (_WIN32) && !defined (_MSC_VER)
    00135 #    include <sys/times.h>
    00136 #  endif
    00137 #  ifdef _MSC_VER
    00138 #    define NOUNISTD
    00139 #  endif
    00140 #endif
    00141 
    00142 #include <stddef.h>
    00143 #include <string.h>
    00144 
    00145 #ifndef NOUNISTD
    00146 #  include <unistd.h>
    00147 #else
    00148 #  ifndef VMS 
    00149 #    ifndef _MSC_VER
    00150 #      include <sys/file.h>     /* ESV, old Concentrix */ /* non-POSIX */
    00151 #    endif
    00152 #  endif
    00153 #endif
    00154 #ifndef NOSTDLIB                /* for TitanOS 4.2, at least? */
    00155 #  include <stdlib.h>
    00156 #endif
    00157 
    00158 #include <errno.h>
    00159 #include <ctype.h>
    00160 
    00161 #if defined(_AIX) || defined (__hpux) || defined(F2C) ||\
    00162     defined(G77) || defined(_WIN32) || defined (sun) /* would do no harm on others, though */
    00163 #  include <time.h>
    00164 #endif
    00165 
    00166 #include <limits.h>
    00167 #include <float.h>
    00168 
    00169 #if defined (F2C)
    00170 #  define Skip_f2c_Undefs
    00171 #  include "f2c.h"
    00172 #endif
    00173 #if defined (G77)
    00174 #  define Skip_f2c_Undefs       /* g2c.h infelicity... */
    00175 #  if defined (HAVE_G2C_H)
    00176 #    include "g2c.h"
    00177 #  else
    00178 #    include "f2c.h"
    00179 #  endif
    00180 #endif
    00181 
    00182 /* rint() function does not seen to exist for mingw32
    00183    defined in library_utils.c */
    00184 #  if ((defined _WIN32) || (defined _MSC_VER)) && (!defined rint)
    00185   double rint(double x);
    00186 #endif
    00187 
    00188 #ifdef _MSC_VER
    00189 #define  M_PI            3.14159265358979323846
    00190 #endif
    00191 
    00192 #ifdef _MSC_VER
    00193 #  define PATH_SEPARATOR '\\'
    00194 #  define EXT_SEPARATOR '.'
    00195 #else
    00196 #  define PATH_SEPARATOR '/'
    00197 #  define EXT_SEPARATOR '.'
    00198 #endif
    00199 
    00200 #define MAXFLEN       512    
    00201 #define MAXFILES       16    
    00202 #define DEFMODE         2    
    00204 #define IRRELEVANT_OP   0
    00205 #define READ_OP         1
    00206 #define WRITE_OP        2
    00207 
    00208 #include<fcntl.h>
    00209 #ifndef SEEK_SET
    00210 #  define SEEK_SET 0
    00211 #  define SEEK_CUR 1
    00212 #  define SEEK_END 2
    00213 #endif /* ! SEEK_SET */
    00214 #ifndef O_WRONLY
    00215 #define O_RDONLY 0x0000       
    00216 #define O_WRONLY 0x0001       
    00217 #define O_RDWR   0x0002       
    00218 #define O_APPEND 0x0008       
    00219 #define O_CREAT  0x0200       
    00220 #define O_TRUNC  0x0400       
    00221 #endif
    00222 #define O_TMP    0x0010       
    00224 #define BYTE  0
    00225 #define INT16 1   
    00226 #define INT32 6
    00227 #define FLOAT32 2
    00228 #define COMP32  3
    00229 #define COMP64  4
    00230 
    00231 #define DFNTI_MBO       1       
    00232 #define DFNTI_IBO       4       
    00234 #define DFNTF_BEIEEE    1       
    00235 #define DFNTF_VAX       2       
    00236 #define DFNTF_CONVEXNATIVE 5    
    00237 #define DFNTF_LEIEEE    4       
    00239 #if defined (VAX) || defined (vax) /* gcc seems to use vax */
    00240 #  define NATIVEFT DFNTF_VAX
    00241 #  define NATIVEIT DFNTI_IBO
    00242 #endif
    00243 
    00244 #if defined(MIPSEL) || defined(i386) || defined(i860) || defined(__ia64__) || defined(__amd64__) || defined(__x86_64__)
    00245 #  define NATIVEIT DFNTI_IBO
    00246 #  define NATIVEFT DFNTF_LEIEEE
    00247 #endif
    00248 
    00249 #if defined (powerpc) || defined (__ppc__) || defined __PPC
    00250 #  define NATIVEIT DFNTI_MBO
    00251 #  define NATIVEFT DFNTF_BEIEEE
    00252 #endif
    00253 
    00254 #ifdef __alpha
    00255 #  ifdef VMS
    00256 #    if __IEEE_FLOAT == 1
    00257 #      define NATIVEFT DFNTF_LEIEEE
    00258 #    else
    00259 #      define NATIVEFT DFNTF_VAX
    00260 #    endif
    00261 #  else                       /* assume OSF/1 */
    00262 #    define NATIVEFT DFNTF_LEIEEE
    00263 #  endif
    00264 #  define NATIVEIT DFNTI_IBO
    00265 #endif
    00266 
    00267 #if defined(MIPSEB) || defined(__hpux) || defined(_AIX) || defined(m68k) || defined(mc68000) || defined(sparc) || defined (__sparc__)
    00268 #  define NATIVEIT DFNTI_MBO
    00269 #  define NATIVEFT DFNTF_BEIEEE
    00270 #endif
    00271 
    00272 #ifndef NATIVEFT
    00273 #  error "Can't determine machine number format"
    00274 #endif
    00275 
    00276 #define DFNT_UINT       0       
    00277 #define DFNT_SINT       1       
    00278 #define DFNT_INT        2       
    00279 #define DFNT_UCHAR      3       
    00280 #define DFNT_CHAR       4       
    00281 #define DFNT_FLOAT      5       
    00282 #define DFNT_DOUBLE     6       
    00284 #endif
    00285 
    00286 #endif /* __CCP4_BITS */

.. raw:: html

   </div>
