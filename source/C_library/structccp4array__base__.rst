`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4array\_base\_ Struct Reference
==================================

``#include <ccp4_array.h>``

`List of all members. <structccp4array__base__-members.html>`__

| 

Public Attributes
-----------------

 int 

**size**

 int 

**capacity**

--------------

Detailed Description
--------------------

struct definition for the array pre-header

--------------

The documentation for this struct was generated from the following file:

-  `ccp4\_array.h <ccp4__array_8h-source.html>`__
