`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_parser.c File Reference
=============================

Functions to read in and "parse" CCP4-style keyworded input.
`More... <#_details>`__

| ``#include <stdio.h>``
| ``#include <stdlib.h>``
| ``#include <string.h>``
| ``#include <float.h>``
| ``#include <math.h>``
| ``#include "ccp4_parser.h"``
| ``#include "ccp4_errno.h"``
| ``#include "ccp4_sysdep.h"``

| 

Defines
-------

 #define 

**CPARSER\_ERRNO**\ (n)   (CCP4\_ERR\_PARS \| (n))

 #define 

**CPARSERR\_Ok**   0

 #define 

**CPARSERR\_MaxTokExceeded**   1

 #define 

**CPARSERR\_AllocFail**   2

 #define 

**CPARSERR\_NullPointer**   3

 #define 

**CPARSERR\_LongLine**   4

 #define 

**CPARSERR\_CantOpenFile**   5

 #define 

**CPARSERR\_NoName**   6

 #define 

**CPARSERR\_ExpOverflow**   7

 #define 

**CPARSERR\_ExpUnderflow**   8

 #define 

**CPARSERR\_MatToSymop**   9

 #define 

**CPARSERR\_SymopToMat**   10

| 

Functions
---------

CCP4PARSERARRAY \* 

`ccp4\_parse\_start <ccp4__parser_8c.html#a13>`__ (const int maxtokens)

int 

`ccp4\_parse\_end <ccp4__parser_8c.html#a14>`__ (CCP4PARSERARRAY
\*parsePtr)

 int 

**ccp4\_parse\_init\_token** (const CCP4PARSERARRAY \*parsePtr, const
int itok)

 int 

**ccp4\_parse\_reset** (CCP4PARSERARRAY \*parsePtr)

 int 

**ccp4\_parse\_delimiters** (CCP4PARSERARRAY \*parsePtr, const char
\*delim, const char \*nulldelim)

 int 

**ccp4\_parse\_comments** (CCP4PARSERARRAY \*parsePtr, const char
\*comment\_chars)

 int 

**ccp4\_parse\_maxmin** (CCP4PARSERARRAY \*parsePtr, const double
max\_exponent, const double min\_exponent)

 int 

**ccp4\_parse** (const char \*line, CCP4PARSERARRAY \*parser)

int 

`ccp4\_parser <ccp4__parser_8c.html#a21>`__ (char \*line, const int
nchars, CCP4PARSERARRAY \*parser, const int print)

int 

`ccp4\_keymatch <ccp4__parser_8c.html#a22>`__ (const char \*keyin1,
const char \*keyin2)

char \* 

`strtoupper <ccp4__parser_8c.html#a23>`__ (char \*str1, const char
\*str2)

char \* 

`strtolower <ccp4__parser_8c.html#a24>`__ (char \*str1, const char
\*str2)

 int 

**strmatch** (const char \*str1, const char \*str2)

 int 

**charmatch** (const char character, const char \*charlist)

 int 

**doublefromstr** (const char \*str, const double max\_exp, const double
min\_exp, double \*valuePtr, double \*intvaluePtr, int \*intdigitsPtr,
double \*frcvaluePtr, int \*frcdigitsPtr, double \*expvaluePtr, int
\*expdigitsPtr)

ccp4\_symop 

`symop\_to\_rotandtrn <ccp4__parser_8c.html#a28>`__ (const char
\*symchs\_begin, const char \*symchs\_end)

const char \* 

`symop\_to\_mat4 <ccp4__parser_8c.html#a29>`__ (const char
\*symchs\_begin, const char \*symchs\_end, float \*rot)

 int 

**symop\_to\_mat4\_err** (const char \*symop)

 ccp4\_symop 

**mat4\_to\_rotandtrn** (const float rsm[4][4])

 char \* 

**rotandtrn\_to\_symop** (char \*symchs\_begin, char \*symchs\_end,
const ccp4\_symop symop)

 void 

**rotandtrn\_to\_mat4** (float rsm[4][4], const ccp4\_symop symop)

char \* 

`mat4\_to\_symop <ccp4__parser_8c.html#a34>`__ (char \*symchs\_begin,
char \*symchs\_end, const float rsm[4][4])

char \* 

`mat4\_to\_recip\_symop <ccp4__parser_8c.html#a35>`__ (char
\*symchs\_begin, char \*symchs\_end, const float rsm[4][4])

--------------

Detailed Description
--------------------

Functions to read in and "parse" CCP4-style keyworded input.

 **Author:**
    Peter Briggs

 **Date:**
    April 2001

--------------

Function Documentation
----------------------

int ccp4\_keymatch

( 

const char \* 

  *keyin1*,

const char \* 

  *keyin2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test whether two keywords are        |
|                                      | identical. Keywords are identical if |
|                                      | they are the same up to the first    |
|                                      | four characters, independent of      |
|                                      | case.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+--------------+   |
|                                      |     | *keyin1*    | keyword 1.   |   |
|                                      |     +-------------+--------------+   |
|                                      |     | *keyin2*    | keyword 2.   |   |
|                                      |     +-------------+--------------+   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if keywords keyin1 and keyin2  |
|                                      |     are "identical", 0 otherwise.    |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+-----------------------+--------------- |
| -+------+----+                                                           |
| | int ccp4\_parse\_end   | (    | CCP4PARSERARRAY \*    |   *parsePtr*   |
|  | )    |    |                                                           |
| +------------------------+------+-----------------------+--------------- |
| -+------+----+                                                           |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Cleans up a CCP4PARSEARRAY after     |
|                                      | being used by ccp4\_parse/           |
|                                      | ccp4\_parser functions.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ---------------------------+         |
|                                      |     | *parsePtr*    | pointer to a C |
|                                      | CP4PARSERARRAY structure   |         |
|                                      |     +---------------+--------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on completion                  |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------------------+------+--------------+-------- |
| ---------+------+----+                                                   |
| | CCP4PARSERARRAY\* ccp4\_parse\_start   | (    | const int    |   *maxt |
| okens*   | )    |    |                                                   |
| +----------------------------------------+------+--------------+-------- |
| ---------+------+----+                                                   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Initialise a CCP4PARSERARRAY to be   |
|                                      | used in subsequent calls to          |
|                                      | ccp4\_parser routines. The calling   |
|                                      | function must supply the maximum     |
|                                      | number of tokens on a line           |
|                                      | (including continuation lines).      |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ------------------------+            |
|                                      |     | *maxtokens*    | maximum numbe |
|                                      | r of tokens on a line   |            |
|                                      |     +----------------+-------------- |
|                                      | ------------------------+            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to a new CCP4PARSERARRAY |
|                                      |     structure                        |
+--------------------------------------+--------------------------------------+

int ccp4\_parser

( 

char \* 

  *line*,

const int 

  *n*,

CCP4PARSERARRAY \* 

  *parser*,

const int 

  *print*

) 

+--------------------------------------+--------------------------------------+
|                                      | The main function for parsing lines, |
|                                      | either supplied or read from stdin.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *line*      | pointer to a nul |
|                                      | l-terminated string of characters, f |
|                                      | orming the input to be processed. On |
|                                      |  input can either be an empty string |
|                                      |  ("") which forces reading from stdi |
|                                      | n, or contain characters to be proce |
|                                      | ssed. On output "line" will be overw |
|                                      | ritten with the actual input line.   |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *n*         | maximum number o |
|                                      | f characters that can be read into " |
|                                      | line" i.e. the size of "line" in mem |
|                                      | ory.                                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *parser*    | pointer to a CCP |
|                                      | 4PARSERARRAY structure which will be |
|                                      |  used to hold the results of process |
|                                      | ing the input line.                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *print*     | flag controlling |
|                                      |  echoing of input lines to stdout. p |
|                                      | rint=0: suppress echoing of lines to |
|                                      |  stdout. Otherwise echoing is turned |
|                                      |  on.                                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of tokens found.          |
+--------------------------------------+--------------------------------------+

char\* mat4\_to\_recip\_symop

( 

char \* 

  *symchs\_begin*,

char \* 

  *symchs\_end*,

const float 

  *rsm*\ [4][4]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert symmetry operator as matrix  |
|                                      | to string in reciprocal space        |
|                                      | notation. This is Charles' version   |
|                                      | of symtr. Note that translations are |
|                                      | held in elements [\*][3] and [3][3]  |
|                                      | is set to 1.0                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs\_begin*    | pointer t |
|                                      | o beginning of string                |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs\_end*      | pointer t |
|                                      | o end of string (i.e. last character |
|                                      |  is \*(symchs\_end-1) )   |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *rsm*              | 4 x 4 mat |
|                                      | rix operator                         |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to beginning of string   |
+--------------------------------------+--------------------------------------+

char\* mat4\_to\_symop

( 

char \* 

  *symchs\_begin*,

char \* 

  *symchs\_end*,

const float 

  *rsm*\ [4][4]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert symmetry operator as matrix  |
|                                      | to string. This is Charles' version  |
|                                      | of symtr. Note that translations are |
|                                      | held in elements [\*][3] and [3][3]  |
|                                      | is set to 1.0                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs\_begin*    | pointer t |
|                                      | o beginning of string                |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs\_end*      | pointer t |
|                                      | o end of string (i.e. last character |
|                                      |  is \*(symchs\_end-1) )   |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *rsm*              | 4 x 4 mat |
|                                      | rix operator                         |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to beginning of string   |
+--------------------------------------+--------------------------------------+

char\* strtolower

( 

char \* 

  *str1*,

const char \* 

  *str2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert string to lowercase.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *str1*    | On exit str1 will  |
|                                      | contain lowercased copy of str2   |  |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *str2*    | Input string       |
|                                      |                                   |  |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     str1                             |
+--------------------------------------+--------------------------------------+

char\* strtoupper

( 

char \* 

  *str1*,

const char \* 

  *str2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert string to uppercase.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *str1*    | On exit str1 will  |
|                                      | contain uppercased copy of str2   |  |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *str2*    | Input string       |
|                                      |                                   |  |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     str1                             |
+--------------------------------------+--------------------------------------+

const char\* symop\_to\_mat4

( 

const char \* 

  *symchs\_begin*,

const char \* 

  *symchs\_end*,

float \* 

  *rot*

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert symmetry operator as string  |
|                                      | to matrix. This is Charles' version  |
|                                      | of symfr. Note that translations are |
|                                      | held in elements [\*][3] and [3][3]  |
|                                      | is set to 1.0                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs\_begin*    | pointer t |
|                                      | o beginning of string                |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs\_end*      | pointer t |
|                                      | o end of string (i.e. last character |
|                                      |  is \*(symchs\_end-1) )   |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *rot*              | 4 x 4 mat |
|                                      | rix operator                         |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     NULL on error, final position    |
|                                      |     pointer on success               |
+--------------------------------------+--------------------------------------+

ccp4\_symop symop\_to\_rotandtrn

( 

const char \* 

  *symchs\_begin*,

const char \* 

  *symchs\_end*

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert symmetry operator as string  |
|                                      | to ccp4\_symop struct.               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs\_begin*    | pointer t |
|                                      | o beginning of string                |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs\_end*      | pointer t |
|                                      | o end of string (i.e. last character |
|                                      |  is \*(symchs\_end-1) )   |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to ccp4\_symop struct    |
+--------------------------------------+--------------------------------------+
