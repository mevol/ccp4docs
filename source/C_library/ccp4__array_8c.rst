`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_array.c File Reference
============================

| ``#include "ccp4_array.h"``

| 

Functions
---------

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ 

`ccp4array\_new\_ <ccp4__array_8c.html#a1>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p)

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ 

`ccp4array\_new\_size\_ <ccp4__array_8c.html#a2>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p, const int size, const
size\_t reclen)

void 

`ccp4array\_resize\_ <ccp4__array_8c.html#a3>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p, const int size, const
size\_t reclen)

void 

`ccp4array\_reserve\_ <ccp4__array_8c.html#a4>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p, const int size, const
size\_t reclen)

void 

`ccp4array\_append\_ <ccp4__array_8c.html#a5>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p,
`ccp4\_constptr <ccp4__array_8h.html#a14>`__ data, const size\_t reclen)

void 

`ccp4array\_append\_n\_ <ccp4__array_8c.html#a6>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p,
`ccp4\_constptr <ccp4__array_8h.html#a14>`__ data, const int n, const
size\_t reclen)

void 

`ccp4array\_append\_list\_ <ccp4__array_8c.html#a7>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p,
`ccp4\_constptr <ccp4__array_8h.html#a14>`__ data, const int n, const
size\_t reclen)

void 

`ccp4array\_insert\_ <ccp4__array_8c.html#a8>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p, const int i,
`ccp4\_constptr <ccp4__array_8h.html#a14>`__ data, const size\_t reclen)

void 

`ccp4array\_delete\_ordered\_ <ccp4__array_8c.html#a9>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p, const int i, const size\_t
reclen)

void 

`ccp4array\_delete\_ <ccp4__array_8c.html#a10>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p, const int i, const size\_t
reclen)

void 

`ccp4array\_delete\_last\_ <ccp4__array_8c.html#a11>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p, const size\_t reclen)

int 

`ccp4array\_size\_ <ccp4__array_8c.html#a12>`__
(`ccp4\_constptr <ccp4__array_8h.html#a14>`__ \*p)

void 

`ccp4array\_free\_ <ccp4__array_8c.html#a13>`__
(`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \*p)

--------------

Detailed Description
--------------------

implementation file for resizable array implementation. Kevin Cowtan

--------------

Function Documentation
----------------------

void ccp4array\_append\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

`ccp4\_constptr <ccp4__array_8h.html#a14>`__ 

  *data*,

const size\_t 

  *reclen*

) 

+-----+-------------------------------+
|     | See macro ccp4array\_append   |
+-----+-------------------------------+

void ccp4array\_append\_list\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

`ccp4\_constptr <ccp4__array_8h.html#a14>`__ 

  *data*,

const int 

  *n*,

const size\_t 

  *reclen*

) 

+-----+-------------------------------------+
|     | See macro ccp4array\_append\_list   |
+-----+-------------------------------------+

void ccp4array\_append\_n\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

`ccp4\_constptr <ccp4__array_8h.html#a14>`__ 

  *data*,

const int 

  *n*,

const size\_t 

  *reclen*

) 

+-----+----------------------------------+
|     | See macro ccp4array\_append\_n   |
+-----+----------------------------------+

void ccp4array\_delete\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

const int 

  *i*,

const size\_t 

  *reclen*

) 

+-----+-------------------------------+
|     | See macro ccp4array\_delete   |
+-----+-------------------------------+

void ccp4array\_delete\_last\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

const size\_t 

  *reclen*

) 

+-----+-------------------------------------+
|     | See macro ccp4array\_delete\_last   |
+-----+-------------------------------------+

void ccp4array\_delete\_ordered\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

const int 

  *i*,

const size\_t 

  *reclen*

) 

+-----+----------------------------------------+
|     | See macro ccp4array\_delete\_ordered   |
+-----+----------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------------------------- |
| ----------+---------+------+----+                                        |
| | void ccp4array\_free\_   | (    | `ccp4\_ptr <ccp4__array_8h.html#a16> |
| `__ \*    |   *p*   | )    |    |                                        |
| +--------------------------+------+------------------------------------- |
| ----------+---------+------+----+                                        |
+--------------------------------------------------------------------------+

+-----+-----------------------------+
|     | See macro ccp4array\_free   |
+-----+-----------------------------+

void ccp4array\_insert\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

const int 

  *i*,

`ccp4\_constptr <ccp4__array_8h.html#a14>`__ 

  *data*,

const size\_t 

  *reclen*

) 

+-----+-------------------------------+
|     | See macro ccp4array\_insert   |
+-----+-------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------------------------------------+------+--- |
| --------------------------------------------+---------+------+----+      |
| | `ccp4\_ptr <ccp4__array_8h.html#a16>`__ ccp4array\_new\_   | (    | `c |
| cp4\_ptr <ccp4__array_8h.html#a16>`__ \*    |   *p*   | )    |    |      |
| +------------------------------------------------------------+------+--- |
| --------------------------------------------+---------+------+----+      |
+--------------------------------------------------------------------------+

+-----+----------------------------+
|     | See macro ccp4array\_new   |
+-----+----------------------------+

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ ccp4array\_new\_size\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

const int 

  *size*,

const size\_t 

  *reclen*

) 

+-----+----------------------------------+
|     | See macro ccp4array\_new\_size   |
+-----+----------------------------------+

void ccp4array\_reserve\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

const int 

  *size*,

const size\_t 

  *reclen*

) 

+-----+--------------------------------+
|     | See macro ccp4array\_reserve   |
+-----+--------------------------------+

void ccp4array\_resize\_

( 

`ccp4\_ptr <ccp4__array_8h.html#a16>`__ \* 

  *p*,

const int 

  *size*,

const size\_t 

  *reclen*

) 

+-----+-------------------------------+
|     | See macro ccp4array\_resize   |
+-----+-------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| --------------+---------+------+----+                                    |
| | int ccp4array\_size\_   | (    | `ccp4\_constptr <ccp4__array_8h.html# |
| a14>`__ \*    |   *p*   | )    |    |                                    |
| +-------------------------+------+-------------------------------------- |
| --------------+---------+------+----+                                    |
+--------------------------------------------------------------------------+

+-----+-----------------------------+
|     | See macro ccp4array\_size   |
+-----+-----------------------------+
