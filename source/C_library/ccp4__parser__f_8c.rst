`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_parser\_f.c File Reference
================================

Fortran API to `ccp4\_parser.c <ccp4__parser_8c.html>`__.
`More... <#_details>`__

| ``#include "ccp4_sysdep.h"``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_parser.h"``
| ``#include "ccp4_general.h"``

| 

Defines
-------

 #define 

**PARSER\_DEBUG**\ (x)

| 

Functions
---------

 int 

**fparse\_isblank** (const char \*line, const int line\_len)

 int 

**fparse\_strncpypad** (char \*fstr, const char \*cstr, const int lfstr)

 int 

**fparse\_populate\_arrays** (CCP4PARSERARRAY \*parser, int \*ibeg, int
\*iend, int \*ityp, float \*fvalue, fpstr cvalue, int cvalue\_len, int
\*idec)

 int 

**fparse\_delimiters** (CCP4PARSERARRAY \*parser, char
\*new\_delimiters, char \*new\_nulldelimiters)

  

**FORTRAN\_SUBR** (PARSER, parser,(fpstr key, fpstr line, int \*ibeg,
int \*iend, int \*ityp, float \*fvalue, fpstr cvalue, int \*idec, int
\*ntok, ftn\_logical \*lend, const ftn\_logical \*print, int key\_len,
int line\_len, int cvalue\_len),(fpstr key, fpstr line, int \*ibeg, int
\*iend, int \*ityp, float \*fvalue, fpstr cvalue, int \*idec, int
\*ntok, ftn\_logical \*lend, const ftn\_logical \*print),(fpstr key, int
key\_len, fpstr line, int line\_len, int \*ibeg, int \*iend, int \*ityp,
float \*fvalue, fpstr cvalue, int cvalue\_len, int \*idec, int \*ntok,
ftn\_logical \*lend, const ftn\_logical \*print))

  

**FORTRAN\_SUBR** (PARSE, parse,(fpstr line, int \*ibeg, int \*iend, int
\*ityp, float \*fvalue, fpstr cvalue, int \*idec, int \*n, int
line\_len, int cvalue\_len),(fpstr line, int \*ibeg, int \*iend, int
\*ityp, float \*fvalue, fpstr cvalue, int \*idec, int \*n),(fpstr line,
int line\_len, int \*ibeg, int \*iend, int \*ityp, float \*fvalue, fpstr
cvalue, int cvalue\_len, int \*idec, int \*n))

  

**FORTRAN\_SUBR** (PARSDL, parsdl,(fpstr newdlm, int \*nnewdl, int
\*nspecd, int newdlm\_len),(fpstr newdlm, int \*nnewdl, int
\*nspecd),(fpstr newdlm, int newdlm\_len, int \*nnewdl, int \*nspecd))

--------------

Detailed Description
--------------------

Fortran API to `ccp4\_parser.c <ccp4__parser_8c.html>`__.

 **Author:**
    Peter Briggs
