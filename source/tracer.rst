TRACER (CCP4: Supported Program)
================================

NAME
----

**tracer** - Lattice TRAnsformation and CEll Reduction

SYNOPSIS
--------

| **tracer**
| [`Keyworded input <#keywords>`__]

 DESCRIPTION
------------

The reduced cell is the unit cell with the smallest volume whose axes
are the three shortest non-coplanar translations in the lattice. TRACER
converts the cell given to the reduced cell. Reduction to a triclinic
lattice with c<a<b and angles alpha and beta non-acute allows for the
identification of the Bravais lattice of highest symmetry which it
represents; this is done by calculation of the 6 scalar products a.a,
a.b ... and comparison with matrices set up within the program. There
are only 43 independent cells to consider.

 KEYWORDED INPUT
----------------

Available keywords are:

    `**ACENTRED** <#acentred>`__, `**BCENTRED** <#bcentred>`__,
    `**CALCULATE** <#calculate>`__, `**CCENTRED** <#ccentred>`__,
    `**CELL** <#cell>`__, `**DEL** <#del>`__, `**END** <#end>`__,
    `**FCENTRED** <#fcentred>`__, `**MATRIX** <#matrix>`__,
    `**RCELL** <#rcell>`__, `**REDUCED** <#reduced>`__,
    `**TITLE** <#title>`__, `**UCENTRED** <#ucentred>`__.

 TITLE <title>
~~~~~~~~~~~~~~

A title, maximum of 70 characters.

 DEL <del>
~~~~~~~~~~

<del> is the absolute value of the largest permissible difference
between any two of the six reduced cell scalar products. Defaults to
1.0.

The <del> value imposes an upper limit on the equivalence of the special
relationships between the scalar products. Usually the higher the
standard deviation of cell parameters, the higher the value of <del>
that should be used. For lattice parameters with errors of 1 part in
1000, a value for <del> of 0.5-1.0 is advised; for proteins, use at
least 1.0. It is sensible to try more than one value and examine the
results.

 CELL <a> <b> <c> [ <alpha> <beta> <gamma> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cell parameters (angles default to 90 degrees).

 RCELL <a\*> <b\*> <c\*> <alpha\*> <beta\*> <gamma\*>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Reciprocal cell parameters. Don't omit the angles.

 END
~~~~

Terminate input.

 Transformation commands
~~~~~~~~~~~~~~~~~~~~~~~~

The following commands specify transformations of one cell to another.
*They are applied in the order in which they are given.* A sequence of
transformations is terminated by a REDUCED or CALCULATE command. After
this, new ones may be started. Each transformation has an optional
<tag>, which is up to 25 characters of information about it read from
the rest of the line.

 MATRIX <element> ... [ <tag> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Transform this cell to a new cell using the 9 supplied matrix elements
in the order (1,1), (2,1) ... (2,3), (3,3).

 ACENTRED [ <tag> ]
~~~~~~~~~~~~~~~~~~~

The initial cell is A-centred; transform it to primitive cell.

 BCENTRED [ <tag> ]
~~~~~~~~~~~~~~~~~~~

 CCENTRED [ <tag> ]
~~~~~~~~~~~~~~~~~~~

 UCENTRED [ <tag> ]
~~~~~~~~~~~~~~~~~~~

 FCENTRED [ <tag> ]
~~~~~~~~~~~~~~~~~~~

These are analogous to `ACENTRED <#acentred>`__.

 REDUCED [ <tag> ]
~~~~~~~~~~~~~~~~~~

Get reduced cell; identify and generate from it the unit cell of highest
symmetry and its corresponding lattice parameters.

 CALCULATE [ <tag> ]
~~~~~~~~~~~~~~~~~~~~

Do not transform this cell. This option may be used to identify the
transformed cell and calculate its inverse cell parameters.

 EXAMPLE
--------

::

    TITLE tracer test
    DEL 0.1 ! allowed discrepancy
    CELL 10.51 15.15 6.54 90 151.7 90 ! cell parameters
    CCENTRED ! supplied cell is C-centred
    REDUCED ! find reduced cell
    END      

AUTHOR
------

S. L. Lawton
