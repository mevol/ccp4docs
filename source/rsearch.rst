RSEARCH (CCP4: Deprecated Program)
==================================

NAME
----

**rsearch** - R-factor and correlation coefficient between Fcalc and
Fobs for positions of molecule within a specified grid.

SYNOPSIS
--------

| **rsearch** **HKLIN** *foo\_in.mtz* [ **SEARCHSAVE** *foo\_search.tmp*
  ] [ **MAPOUT** *foo\_rsearch.map* ]
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

When a molecule is correctly orientated in a unit cell, but the
translation parameters are not known, RSEARCH calculates the R-factor
and correlation coefficient between calculated and observed structure
factors for each position of the molecule within a specified grid.

You may prefer to calculate a translation function with TFFC instead of
RSEARCH, since this is much faster.

There is an option to restart RSEARCH when it runs out of time : a file
assigned to SEARCHSAVE will record the intermediate sums for the R
factors. These are updated after every 100th reflection and a note is
written to the log file. There are also options (keywords CENT and
RZONE) to limit the reflections used to centrics or a specified zone.

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords. Only the
first 4 characters need be given. Those available are :

    `**AXIS** <#axis>`__, `**BINMAP** <#binmap>`__,
    `**BGRV** <#bgrv>`__, `**CENT** <#cent>`__,
    `**CORRELATION** <#correlation>`__, `**END** <#end>`__,
    `**FCLIMIT** <#fclim>`__, `**FOLIMITS** <#folim>`__,
    `**LABIN** <#labin>`__, `**LPMAP** <#lpmap>`__,
    `**NRFACTOR** <#nrfactor>`__, `**RESOLUTION** <#resolution>`__,
    `**RESTART** <#restart>`__, `**RZONE** <#rzone>`__,
    `**SCALE** <#scale>`__, `**SYMMETRY** <#symmetry>`__,
    `**TITLE** <#title>`__, `**XGRID** <#xgrid>`__,
    `**YGRID** <#ygrid>`__, `**ZGRID** <#zgrid>`__

AXIS
~~~~

| Order of grid axes : fast, medium, slow.
| At present, the program only looks at the slow (sectioning) axis.
  Possible inputs are therefore

::

                                   - X Y Z
                                   - Z X Y
                                   - Y Z X 

BINMAP
~~~~~~

Output a map of R factors or correlation coefficients over the specified
grid

BGRV <brgval>
~~~~~~~~~~~~~

| Background R value for maps.
| Default: the average R value.
| The terms calculated are (background R value - grid R value).

CENT
~~~~

By default, all reflections are included in the calculation. If this is
set, ONLY centric reflections are selected. Centric zones are identified
using the spacegroup operators.

CORRELATION
~~~~~~~~~~~

Turns ON the plotting of correlation maps

FCLIMIT <fmaxc>
~~~~~~~~~~~~~~~

Set lower limit on FC for inclusion in R factor calculation.

In low symmetry space groups such as P21, you should exclude all terms
where one calculated (partial) structure factor FC is very small, since
then the R factor will be independent of translation.

FOLIMITS <fmin> <fmax>
~~~~~~~~~~~~~~~~~~~~~~

Set upper and lower limits on observed structure factors for inclusion
in R factor calculation (given in either order).

LABIN <program label>=<file label>...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input column assignments. The program labels are:

::

            H  K  L  FP  SIGFP  FC1  PC1 ... FC12  PC12  FC  PHIC

FP is the observed structure factor and SIGFP its standard error. FCn
and PCn are the partial calculated structure factors and phases for
symmetry operation n. Known structure may be added in by defining FC and
PHIC.

LPMAP
~~~~~

Output a lineprinter map of R factors for all hkl and for each of 5
resolution shells.

NRFACTOR <nrfactor>
~~~~~~~~~~~~~~~~~~~

Number of R factors to be listed to output file. Default is 15; maximum
is 500.

RESOLUTION [ <resmin> ] <resmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Low and high resolution limits in either order. These are in Angstroms
or 4(sintheta/lambda)\*\*2 if both are <1.0. Defaults: 1000.0 2.0

RESTART <nref> <ihh> <ikk> <ill>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Intermediate results will have already been written to a file assigned
to SEARCHSAVE. Calculation is restarted after the <nref>-th reflection;
<ihh> <ikk> <ill> are the indices of the first reflection to be
included. These 4 numbers are printed to the log file after every 100
reflections.

RZONE <number>...
~~~~~~~~~~~~~~~~~

5 numbers to define a reflection zone limiting reflections included in
the R value calculation.

::

    To define the zone -h +k +l = 3n     input RZON    -1 1 1   3 0
    To define                 l = 2n +1  input RZON     0 0 1   2 1

This keyword may be given more than once, but at present ALL conditions
must be obeyed simultaneously.

SCALE <scale> <bfact>
~~~~~~~~~~~~~~~~~~~~~

| The scale and temperature factor to be applied to observed structure
  factors FP. Defaults: 1.0 0.0.
| FP(USED) is scale\*FP(INPUT)\*EXP(-bfact(SINTHETA/LAMBDA)\*\*2).
| It is sensible to run `SCALEIT <scaleit.html>`__ to put FP onto the
  same scale as one of the (partial) structure factors FCn before
  running RSEARCH. The scale here should then be approximately
  sqrt(nsymp), where nsymp is the number of primitive symmetry
  operators.

SYMMETRY [ <nspgrp> \| <namspg> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Spacegroup number or name. You may alternatively input operators in
X,Y,Z form. Spacegroup details are normally taken from the input MTZ
file.

TITLE <title>
~~~~~~~~~~~~~

A title to be written to the log file.

GRID SPECIFICATIONS
~~~~~~~~~~~~~~~~~~~

XGRID \| YGRID \| ZGRID <min> <max> <n>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

3 parameters for each of the 3 keywords: XGRID, YGRID and ZGRID.

The input is complicated to allow greater flexibility in describing the
search volume. The first 2 parameters give the range in grid points,
either directly or by reference to other limits given; the third, the
number of grid points covering the unit cell in the appropriate
direction.

::

       For a simple rectangular box :
        XGRID    ixmin  ixmax  nx    (3 numbers)
        YGRID    iymin  iymax  ny    (3 numbers)
        ZGRID    izmin  izmax  nz    (3 numbers)

       For a prism, there are many options. 
       For example :
        XGRID    ixmin  ixmax  nx    (3 numbers)
        YGRID   xmin  iymax  ny    (1 character string, 2 numbers) 
    (or YGRID    iymin xmax  ny )  (number, character string, number)
    (or YGRID   xmin xmax  ny )  (2 character strings, 1 number)
        ZGRID    izmin  izmax  nz    (3 numbers)

       where xmin means type the letters xmin ....

       For another sort of prism :
        XGRID    ixmin  ixmax  nx    (3 numbers)
        YGRID   xmin  iymax  ny    (1 character string, 2 numbers)
    (or YGRID    iymin xmax  ny )  (number, character string, number)
    (or YGRID     x    x   ny )  (2 character strings, 1 number)
        ZGRID    izmin  izmax  nz    (3 numbers)
    (or ZGRID   xmin  izmax  nz )  (character string, 2 numbers)  
    (or ZGRID    izmin xmax  nz )  (number, character string, number)
    (or ZGRID     x    x   nz )  (2 character strings, 1 number)
    (or ZGRID   ymin  izmax  nz )  (1 character string, 2 numbers)  
    (or ZGRID    izmin ymax  nz )  (number, character string, number)
    (or ZGRID     y    y   nz )  (2 character strings, 1 number)

Note that maps can only be output if the grid is rectangular. The
maximum total grid points in the search volume is 60000. The maximum
number in any one direction is 1024.

END
~~~

End of input.

INPUT AND OUTPUT FILES
----------------------

::

     
    INPUT  : MTZ Data file assigned to HKLIN
            [File containing intermediate sums assigned to SEARCHSAVE]
    OUTPUT :[File for Binary R factor map assigned to MAPOUT]       

    Those in parentheses are optional.

NOTES
-----

Memory allocation
~~~~~~~~~~~~~~~~~

The program allocates memory dynamically. If the default isn't enough
you can increase it by setting the logical name RSEARCH\_NSIZE to the
appropriate value indicated by the error message. (It ought to figure
this out for itself.)

Suggested procedure leading to RSEARCH
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Run `LSQKAB <lsqkab.html>`__ to rotate your search coordinates by
   alpha beta gamma obtained from rotation search.
   DO NOT FORGET to change your CRYSTL and SCALE cards to those of the
   new space group.
   Remember that the rotation angles are relative to a given NCODE. The
   SCALE matrix MUST be consistent with NCODE.
   `COORDCONV <coordconv.html>`__ will generate such a matrix when it
   converts fractional coordinates to PDB format.
#. Run `SFALL <sfall.html>`__ to generate a list of h k l FC PC for all
   +-h +-k l for these coordinates in this cell.
#. Run `CAD <cad.html>`__ to collect together H K L FP SIGFP FC1 PC1 FC2
   PC2 ... for the unique set of intensity data.
#. Run `SCALEIT <scaleit.html>`__ to scale FP to one of the FCn.
#. Run RSEARCH

   ::

       #
       # *****************************************************************
       # 
       #  Run sfall to calculate structure factors
       #
       sfall 
       HKLOUT sfrkall.mtz 
       XYZIN trans_pgk.pdb 
       << END-sfall                     
       TITLE SFS
       MODE  SFCALC XYZIN
       GRID  80 160 160
       RESOLUTION 20 3 
       SYMMETRY 1
       SFSGRP   1
       LABOUT   FC=FC PHIC=AC
       END
       END-sfall
       #
       # *****************************************************************
       # 
       #  Run cad to collect FP and FC's into one file.
       #  Up to this stage, can use the same procedure for TFFC & RSEARCH.
       #  No phase changes made to partial phases.
       #
       cad 
       HKLIN1 allderiv.mtz 
       HKLIN2 sfrkall.mtz 
       HKLOUT cad.mtz 
       << END-cad
       RESOLUTION OVERALL 20 3
       TITLE  resolving the bacillus pgk 
       LABIN  FILE 1 E1=F  E2=SIGF
       CTYPIN FILE 1 E1=F  E2=Q 
       LABIN  FILE 2 E1=FC E2=AC
       CTYPIN FILE 2 E1=U  E2=V
       END
       END-cad
       #
       # *****************************************************************
       #  
       #  Optional step 
       #  Run scaleit to put FP onto the same scale as one of the FC's. 
       #  Remember you will need to use the resolution required for RSEARCH.
       #
       scaleit 
       HKLIN cad.mtz 
       HKLOUT cad_sc.mtz 
       << END-scaleit
       TITLE Scale FP to one FCpart ready for RSEARCH
       RESOLUTION 8 4
       REFINE   isotropic
       LABIN    FP=FC1 SIGFP=FC1 FPH1=F SIGFPH1=SIGF
       CONVERGE ABS 0.0001 TOLR 0.000000001 NCYC 4
       END 
       END-scaleit

EXAMPLES
--------

For previous stages to this, see 'NOTES'

::

     
    #  Rfactor search
    #  Input mtz hkl list of FP SIGFP FC1 PC1 FC2 PC2.....
    #  R factor requires FP scaled to FC. 
    #  Correlation coefficient should be independent of scale.
    #
    rsearch 
    HKLIN cad_sc.mtz 
    MAPOUT dav.map 
    SEARCHSAVE dav.tmp 
    << END-r  
    TITLE Bacillus search in a-c plane 
    SCALE FP 1.414 0
    RESOLUTION  8 4           !  resolution limits
    FOLIMITS 10 10000000000.  !  Fobs limits
    FCLIMIT  50               !  Fc minimum
    CORRELATION 
    BINMAP                    !  binary map output
    AXIS  Y Z X             
    XGRID 0 20 40             !  grid limits 
    YGRID 0 1  1 
    ZGRID 0 40 80 
    LABIN FP=F SIGFP=SIGF FC1=FC1 PC1=AC1 FC2=FC2 PC2=AC2  
    END
    END-r

AUTHOR
------

Eleanor Dodson, York University
