STEREO (CCP4: Supported Program)
================================

NAME
----

**stereo** - Extract coordinates from stereo diagrams

SYNOPSIS
--------

| **stereo xyzin** *coordinate\_file* **xyzout** *coord.brk*
| `[Keyworded input] <#keywords>`__

 DESCRIPTION
------------

The **stereo** program recreates three-dimensional coordinates on the
basis of a pair of two-dimensional ones extracted from a stereo diagram.
The input coordinates are taken from the logical name XYZIN (see
`**FORMAT OF XYZIN** <#format_of_xyzin>`__) into a PDB-format file on
XYZOUT. The output will not necessarily be a sensible PDB file,
especially if the optional information about atom names and residue
types isn't supplied. The programs `**coordconv** <coordconv.html>`__,
**reform**, and `**pdbset** <pdbset.html>`__ may be useful for
manipulating the output.

The input coordinates are expected to be measured (from some publication
for which you don't have the coordinates electronically) with a flatbed
cross-hair digitiser or similar device. If you don't have such a device
you might be able to do this:

-  make a transparency of the picture;
-  stick it against a graphics screen and fire up some drawing program
   from which you can get the data in a sensible form, *e.g.* \`xfig';
-  draw points or lines to trace each stereo view and save the drawing;
-  mangle the drawn points output into a suitable form for input to this
   program (see `below <#format_of_xyzin>`__) using your favourite
   utility.

 KEYWORDED INPUT
----------------

Available keywords are:

    `**CRITERION** <#criterion>`__, `**CONSTRAINT** <#constraint>`__,
    `**PHI** <#phi>`__, `**TITLE** <#title>`__,
    `**VIEWSET** <#viewset>`__.

 TITLE <title>
~~~~~~~~~~~~~~

 PHI <phi>
~~~~~~~~~~

Angular separation of the stereo components in degrees. If not given the
correct value will be searched for.

 VIEWSET <v>
~~~~~~~~~~~~

Viewing distance in inches. If not given the correct value will be
searched for.

 CRITERION <crit>
~~~~~~~~~~~~~~~~~

Criterion for removing bad measurements from <phi> and <v> searches. The
default value is 1.5. Prior to <phi>, <v> search, the program uses
<phi>=3 degrees, <v>=infinity, and distances used in constraints. All
distances which are outside the range (s\*<crit>,s/(<crit>\*\*2)), where
s is the standard Ca-Ca distance of 3.84 A, will be rejected from the
search.

 CONSTRAINT <const1> <const2> <const3> <const4>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Specify a constraint. If no constraints are specified a polypeptide
chain of alpha carbon atoms will be assumed and the distances between
succeeding pairs of atoms will be refined against the standard alpha
carbon - alpha carbon distance of 3.84 Angstroms.

 <const1>
    First i-th atom in constraint
 <const2>
    First j-th atom in constraint (if negative, will count backwards).
 <const3>
    Last i-th atom in constraint
 <const4>
    Distance s(i,j) between i-th and j-th atoms measured in Angstroems.

If <const2>=0 for the k-th constraint line, an alpha helix is assumed.
In this case <const1> should be the first carbon alpha atom in the helix
(call it atom i) and <const3> should be the last carbon alpha atom in
the helix (call it atom j) and a series of constraints is generated in
place of the given constraint as follows.

 The first set of constraints will be for
    ::

        atom i to atom i+2
        atom i+1 to atom i+3                                           
        etc. until i+n=j

 The second set of constraints will be for
    ::

        atom i to atom i+3                                             
        atom i+1 to atom i+4                                           
        etc. until i+n=j                                               
        the last constraint will be for                                    
        atom i to atom j                                               

By assuming the parameters for an alpha helix, the program works out the
appropriate values for s(i,j) in subroutine HELIX.

Examples of particular constraints that may be set are

#. An alpha carbon chain of n atoms.
   CONSTRAINT 1 2 n-1 3.84
#. Two antiparallel strands, each n residues long, starting at residue
   numbers p and q.
   CONSTRAINT p -(q+n-1) (p+n-1) 4.72 (Note: there are two different
   Ca-Ca distances between antiparallel strands and this number is an
   average.)
#. An alpha helix n residues long starting at residue p.
   CONSTRAINT p p+4 p+n-4 6.20

| The program will minimise the sum over all the constraints of
| sum(((x(i)-x(j))\*\*2-s(i,j)\*\*2)\*\*2)

 FORMAT OF XYZIN
----------------

| The input data file comprises free format records of the form
| <xl> <yl> <xr> <yr> [ <atom name> [ <residue name> [ <residue number>]
  ] ]

These are the left and right stereo diagram coordinates. It is assumed
that the coordinates will be given in microns, so as to provide the
viewing distance in inches. The program will work if the coordinates are
in any other units but the <v> distance will not be in inches but on a
corresponding arbitrary scale. Each coordinate has an optional <atom
name>, <residue name> and <residue number> which will be used in he
XYZOUT file if given. Otherwise dummy tags will be used.

 STANDARD OUTPUT
----------------

The output is hopefully self explanatory but essentially it consists of
the following sequential items following header.

-  Echo of input data
-  Refinement of orientation of x,y coordinates to minimise y separation
   in left and right equivalent atoms.
-  x and y refined to separate origins.
-  Statistics on separation of x and y coordinates. initial
   determination of <phi>.
-  Search for best <phi> and <v>.
-  Best value of <phi> and <v>.
-  x, y, z three-dimensional coordinates for each cycle of refinement.

   ::

                 a. 3 cycles of z refinement                                     
                 b. 3 cycles of y refinement                                     
                 c. 3 cycles of x refinement                                     

   Shifts (in Angstroms), coordinates and rms for each constraint are
   given after each cycle.

SEE ALSO
--------

`pdbset <pdbset.html>`__, reform (1), xfig (1)

AUTHOR
------

::

    M. G. Rossmann                             March 1979 
            Dept. of Biological Sciences                                      
            Purdue University                                                 
            West Lafayette, Indiana 47907                                     

    Modified and reorganized with new math routines
            b4x@mace.cc.purdue.edu         August 5, 1994

    Hacked into CCP4 conventions (basically i/o) at Daresbury.
