PISA (CCP4: Supported Program)
==============================

NAME
----

**pisa** - Protein interfaces, surfaces and assemblies

USAGE
-----

#. Perform PISA analysis of a structure given in PDB or mmCIF file
   'coorfile':

   ::

        # ./pisa name -analyse coorfile [cfg]
        

   where 'name' is a mandatory session name, [cfg] stands for optional
   configuration file. Configuration file must be specified unless
   pointed out by environmental variable PISA\_CONF\_FILE. After
   finishing, the results will be stored in session directory identified
   by session name 'name'. Use the following commands for retrieving the
   results.

#. Retrieve lists of interfaces, monomers and assemblies:

   ::

        # ./pisa name -list {interfaces|monomers|assemblies} [cfg]
        

#. View an interface, monomer, assembly or a dissociate:

   ::

        # ./pisa name -view spec serial_no [cfg]
        

   where spec={interface\|monomer\|assembly\|dissociate}, and serial\_no
   is serial number shown in the corresponding list.

#. Download an interface, monomer, assembly or a dissociate:

   ::

        # ./pisa name -download spec serial_no [cfg] > output_file
        

#. Get details of an interface, monomer or assembly:

   ::

        # ./pisa name -detail spec serial_no [cfg]
        

#. Generate Remark 350:

   ::

        # ./pisa name -350 assembly_serial_no [cfg]
        

#. Generate XML output:

   ::

        # ./pisa name -xml [cfg] > outputfile.xml
        

#. Erase session data:

   ::

        # ./pisa name -erase [cfg]
        

DESCRIPTION
-----------

PISA is an interactive tool for the exploration of macromolecular
(protein, DNA/RNA and ligand) interfaces, prediction of probable
quaternary structures (assemblies), database searches of structurally
similar interfaces and assemblies, as well as searches on various
assembly and PDB entry parameters.

Reading the Ouput:
------------------

Problems:
---------

AUTHOR
------

Eugene Krissinel and Kim Henrick, EBI

CITATION
--------

::

    E. Krissinel and K. Henrick (2005). 
    Detection of Protein Assemblies in Crystals. In: M.R. Berthold et.al. (Eds.): CompLife 2005, LNBI 3695, pp. 163--174. Springer-Verlag Berlin Heidelberg.

The citation will change for the following

::

    E. Krissinel and K. Henrick (2007). 
    Inference of macromolecular assemblies from crystalline state. Journal of Molecular Biology, accepted for publication on 08.05.2007.

SEE ALSO
--------


