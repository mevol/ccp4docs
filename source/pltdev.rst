pltdev (CCP4: Supported Program)
================================

NAME
----

**pltdev** - convert Plot84 meta-files to PostScript, Tektronix or HPGL.

SYNOPSIS
--------

**pltdev** [-log] [-del] [-lan] [-por] [-aut] [-abs] [-hea] [-pri] [-i
file] [-o file] [-xp value] [-yp value] [-lnw value] [-lni value] [-pen
line/dash/grey/col] [-sca value] [-n value] [-pnt value] [-P
printer\_id] [-sta value] [-pwd string] [-hel value] [-sh value] [-xs
value] [-ys value] [-sep value] [-ste] [-dev ps/hp7550/hp7470/tex]
*filename*

DESCRIPTION
-----------

**pltdev** converts a Plot84 meta-file to a format that is appropriate
for certain devices. By default the output is directed to a PostScript
file, but it can be directed to a Tektronix screen or HPGL Plotter.

**pltdev** produces output in various sizes and orientations much of
which can be selected by default. It will also produce stereo pairs of
pictures.

OPTIONS
~~~~~~~

Note that the VMS version uses \`/' and \`=' rather than \`-' and space
to delimit options.

 -log
    produce log information (defaults to off)
 -del
    delete file after processing (defaults to off i.e. do not delete
    file)
 -lan
    select landscape format (defaults to undefined). Chooses most
    appropriate orientation if the -lan or -por flags are not explicitly
    given
 -por
    select portrait format (defaults to undefined). Chooses most
    appropriate orientation if the -lan or -por flags are not explicitly
    given
 -aut
    select automatic scaling (defaults to on). Used to prevent the
    interactive enquiry to re-scale if appropriate so should be used for
    background jobs.
 -abs
    select absolute scaling (defaults to off)
 -hea
    produce header line/picture (defaults to off)
 -pri
    print the result (defaults to off). This will send the file to a
    printing device otherwise the output will be left in the output file
 -i file
    input file name (defaults to plot84.plt)
 -o file
    output file name (defaults to plot84.ps)
 -xp num
    start x position (defaults to 0.7). Measured in cm.
 -yp num
    start y position (defaults to 0.7). Measured in cm.
 -lnw num
    line thickness (defaults to 0.8). Initial thickness of lines.
 -lni num
    line increment (defaults to 0.1). Used to simulate colour on single
    pen devices.
 -pen l/d/g/c
    set shade by line/dash/grey/col (defaults to line)
 -sca num
    select scale (defaults to 1.0)
 -n num
    number of pictures (defaults to 500)
 -pnt num
    point thickness (defaults to 1.0)
 -P string
    select printer name (defaults to psp)
 -sta num
    start picture number (defaults to 1)
 -pwd string
    password in meta-file (defaults to PLOT%%84)
 -hel num
    print help information and exit (defaults to 0)
 -sh num
    print colour information and exit; use -1 for all colours (defaults
    to -1)
 -xs num
    overall size in x direction (defaults to 6.5). Measured in cm.
 -ys num
    overall size in y direction (defaults to 6.5). Measured in cm.
 -sep num
    gap between stereo pair in cm (defaults to 0.3). Measured in cm.
 -ste
    select stereo picture mode (defaults to off)
 -dev ps/hp7550/hp7470/tek
    set device ps/hp7550/hp7470/tek (defaults to postscript)

SEE ALSO
--------

`p842asc <p842asc.html>`__, `asc2p84 <p842asc.html>`__

AUTHOR
------

| P.J. Daly, SERC, Daresbury Laboratory, Warrington, WA4 4AD.
| daly@fr.esrf

D. Parry-Jones, Astbury Department of Biophysics, University of Leeds,
England.

BUGS
----

May need to change the orientation values to initially set up for your
devices and may have to edit the initialisation string for your HPGL
Plotter.

The output files can be huge. The PostScript %%BoundingBox comment will
be wrong if not all the pictures fit into the bounding box of the first
one.
