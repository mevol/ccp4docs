|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Result Pages <qtpisa-resultpages.html>`__ •
`List of Interfaces <qtpisa-intflistpage.html>`__ • `Interface Details &
Radar <qtpisa-intfdetpage.html>`__ • Interface residues

.. raw:: html

   </div>

The Interface Residues page lists residues of interfacing
`monomers <qtpisa-glossary.html#monomeric-unit>`__, found in the
interface area. The respective interface may be identified in the
`Result Tree <qtpisa-glossary.html#pisa-result-tree>`__ as the parent
node for the currently displayed page. The residues are listed in two
identical tables, in the left and right halves of the page for 1st and
2nd monomers, respectively (where 1st and 2nd are identified as shown in
the `Interface Details <qtpisa-intfdetpage.html>`__ page). The tables
have the following 5-column format:

+-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------+
| Monomer                                                                                   | HS                                                                                                                           | ASA                                                                                                      | BSA                                                                                                                                                          | Delta G                                                                                           |
+===========================================================================================+==============================================================================================================================+==========================================================================================================+==============================================================================================================================================================+===================================================================================================+
| `Residue ID <qtpisa-glossary.html#residue-id>`__ of the corresponding monomer's residue   | Either H, or S, or both, depending on whether the corresponding residue forms a hydrogen bond or salt bridge, respectively   | `Acessible Surface Area <qtpisa-glossary.html#asa>`__ of the correspondng residue, in square angstroms   | `Buried Surface Area <qtpisa-glossary.html#bsa>`__ of the correspondng residue, in square angstroms. The BSA value corresponds to the inspected interface.   | `Solvation Energy Effect <qtpisa-glossary.html#sef>`__ on the correspondng residue, in kcal/mol   |
+-------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------+

| By default, only interface residues (i.e. those with non-zero
  `BSA <qtpisa-glossary.html#bsa>`__) are listed in the tables. In the
  bottom of the page, there is a compbobox switch, which allows a user
  to inspect all surface residues (i.e. those with non-zero
  `ASA <qtpisa-glossary.html#asa>`__), or all residues in both monomers.
|  

--------------

| ***See also***

-  `Chemical bonds <qtpisa-intfbondspage.html>`__

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
