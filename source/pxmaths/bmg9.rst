|CCP4 web logo|

Basic Maths for Protein Crystallographers

Phasing diagrams

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

SIR - Single Isomorphous Replacement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|SIR phase circles|

Phasing diagram for Single Isomorphous Replacement.

Two circles are drawn, one with radius \|F\ :sub:`P`\ \| centred on the
origin, and one with radius \|F\ :sub:`PH`\ \| about the end of the
vector -**F\ :sub:`H`**. The two points of intersection A and B of the
two circles correspond to two possible values of the protein phase. The
mean or "best" protein phase for such a circle will always equal
|phi|\ (**h**) or |phi|\ (**h**)+\ |pi|. The "figure of merit" is a
measure of the phase error. Here it equals cos\ |phi|\ :sub:`diff` and
indicates how separate the two crossings are. If |phi|\ :sub:`diff` = 90
then the FOM will equal 0.0.

If the heavy atom constellation is on the wrong hand, the resultant
phase estimates for |phi|\ :sub:`P` will also be on the wrong hand, but
the figure of merits will be identical.

+--------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| The phase ambiguities are:                                                           |
+======================================================================================+========================================================================================+
| Crossing                                                                             | Hand                                                                                   |
+--------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| |phi|\ (**h**)\ :sub:`1` + |phi|\ :sub:`diff\ :sub:`1``                              | -|phi|\ (**h**)\ :sub:`1` + |phi|\ :sub:`diff\ :sub:`1``                               |
| |phi|\ (**h**)\ :sub:`1` - |phi|\ :sub:`diff\ :sub:`1``                              | -|phi|\ (**h**)\ :sub:`1` - |phi|\ :sub:`diff\ :sub:`1``                               |
+--------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| |phi|\ :sub:`best` = |phi|\ (**h**)\ :sub:`1` or |phi|\ (**h**)\ :sub:`1`\ +\ |pi|   | |phi|\ :sub:`best` = -|phi|\ (**h**)\ :sub:`1` or -|phi|\ (**h**)\ :sub:`1`\ +\ |pi|   |
+--------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+

Maps from different hands will be enantiomorphic.

MIR - Multiple Isomorphous Replacement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|MIR phase circles|

Phasing diagram for Multiple Isomorphous Replacement.

When two derivatives are available, both with heavy atoms positioned on
the **same origin and hand**, a third circle can be drawn, and in
favourable circumstances the three circles intersect in one clearly
defined point (point A in the diagram), thus resolving the phase
ambiguity.

To make sure the two derivatives have heavy atoms on the same origin and
same hand, it is important to use difference Fouriers to position the
second derivative, using phases for the protein determined from the
first derivative.

In this case no anomalous measurements for the derivatives are
available, and if the heavy atoms structure factor is calculated on the
other hand, a mirror image of the figure across the |phi|\ (**h**)=0
line will be generated, changing all phases to their complex conjugates,
including the resultant phase estimate for the protein. The resultant
map will be the enantiomorph of the first one, so both should show clear
solvent boundaries, and look "good", but one will show features like
left-handed helices.

+-----------------------------------------------------------+------------------------------------------------------------+
| The phase ambiguities are:                                |
+===========================================================+============================================================+
| Crossing                                                  | Hand                                                       |
+-----------------------------------------------------------+------------------------------------------------------------+
| |phi|\ (**h**)\ :sub:`1` + |phi|\ :sub:`diff\ :sub:`1``   | -|phi|\ (**h**)\ :sub:`1` + |phi|\ :sub:`diff\ :sub:`1``   |
| |phi|\ (**h**)\ :sub:`1` - |phi|\ :sub:`diff\ :sub:`1``   | -|phi|\ (**h**)\ :sub:`1` - |phi|\ :sub:`diff\ :sub:`1``   |
+-----------------------------------------------------------+------------------------------------------------------------+
| |phi|\ (**h**)\ :sub:`2` + |phi|\ :sub:`diff\ :sub:`2``   | -|phi|\ (**h**)\ :sub:`2` + |phi|\ :sub:`diff\ :sub:`2``   |
| |phi|\ (**h**)\ :sub:`2` - |phi|\ :sub:`diff\ :sub:`2``   | -|phi|\ (**h**)\ :sub:`2` - |phi|\ :sub:`diff\ :sub:`2``   |
+-----------------------------------------------------------+------------------------------------------------------------+

| In this figure there is a good crossing at
  (|phi|\ :sub:`2`-|phi|\ :sub:`diff\ :sub:`2``,
  |phi|\ :sub:`1`\ +\ |phi|\ :sub:`diff\ :sub:`1``).
| And in the corresponding one for the other hand, this would be at
  (-|phi|\ :sub:`2`\ +\ |phi|\ :sub:`diff\ :sub:`2``,
  -|phi|\ :sub:`1`-|phi|\ :sub:`diff\ :sub:`1``).

SIRAS - Single Isomorphous Replacement with Anomalous Scattering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|SIRAS phase circles|

Phasing diagram for SIR with Anomalous Dispersion.

Referring to the SIR phase, the F\ :sub:`P`\ (**h**) phase will be
|phi|\ :sub:`H`\ (**h**) + |phi|\ :sub:`diff`\ (**h**) or
|phi|\ :sub:`H`\ (**h**) - |phi|\ :sub:`diff`\ (**h**). And for
F\ :sub:`P`\ (-**h**) the phase will be |phi|\ :sub:`H`\ (-**h**) +
|phi|\ :sub:`diff`\ (-**h**) or |phi|\ :sub:`H`\ (-**h**) -
|phi|\ :sub:`diff`\ (-**h**). The anomalous scattering of the heavy
atoms means that |phi|\ :sub:`H`\ (-**h**) is not the complex conjugate
of |phi|\ :sub:`H`\ (**h**). And since \|F\ :sub:`PH`\ (+**h**)\| is not
equal to \|F\ :sub:`PH`\ (-**h**)\| the |phi|\ :sub:`diff`\ (**h**) is
also different from |phi|\ :sub:`diff`\ (-**h**). However the phase of
F\ :sub:`P`\ (-**h**) should equal -phase of F\ :sub:`P`\ (+**h**), so
we can select which of the crossing is the most likely. To represent
this using phasing circles we actually use a trick.

| We consider F\ :sub:`PH`\ (-**h**) as another "derivative"
  F'\ :sub:`PH`\ (**h**) where the heavy atom contribution equals
  F\ :sub:`H\ :sub:`real``\ (**h**)e\ :sup:`i(\ |phi|\ (**h**))` +
  F"\ :sub:`H\ :sub:`imag``\ (**h**) e\ :sup:`i(\ |phi|\ (**h**) - 90)`
  with the anomalous contribution lagging 90 degrees BEHIND the real
  contribution.
| This means that like in the MIR case, there are two centres for
  F\ :sub:`H\ :sub:`i``, but of course they are always rather close.
  Taking F\ :sub:`PH`\ (**h**) as F\ :sub:`PH\ :sub:`1``\ (**h**) and
  F\ :sub:`PH`\ (-**h**) as F\ :sub:`PH\ :sub:`2``\ (**h**), we can
  position the centres of the two "F:sub:`H`" vectors correctly, and in
  this figure the resultant |phi|\ :sub:`P` will equal
  |phi|\ (**h**)\ :sub:`1`\ +\ |phi|\ :sub:`diff\ :sub:`1``, or
  |phi|\ (-**h**)\ :sub:`1`-|phi|\ :sub:`diff\ :sub:`1``. If the heavy
  atoms are on the other hand, |phi|\ (**h**)\ :sub:`2` and
  |phi|\ (-**h**)\ :sub:`2` are no longer the complex conjugates of
  |phi|\ (**h**)\ :sub:`1` and |phi|\ (-**h**)\ :sub:`1` so although the
  crossings will be equally reliable at (|phi|\ (**h**)\ :sub:`2`\ +
  |phi|\ :sub:`diff\ :sub:`1``, |phi|\ (-**h**)\ :sub:`2`-
  |phi|\ :sub:`diff\ :sub:`1``) and the figures of merit will be
  identical, this time the protein phase will not be the enantiomorph.
| Hence maps using the 2 different phases are not equivalent, and you
  should be able to recognise that one is "better" than the other, and
  indicates the correct hand.

+---------------------------------------------------------------------+---------------------------------------------------------------------+
| The phase ambiguities are:                                          |
+=====================================================================+=====================================================================+
| Crossing                                                            | Hand                                                                |
+---------------------------------------------------------------------+---------------------------------------------------------------------+
| |phi|\ (**h**)\ :sub:`1`\ :sup:`+` + |phi|\ :sub:`diff`\ :sup:`+`   | |phi|\ (**h**)\ :sub:`2`\ :sup:`+` + |phi|\ :sub:`diff`\ :sup:`+`   |
| |phi|\ (**h**)\ :sub:`1`\ :sup:`+` - |phi|\ :sub:`diff`\ :sup:`+`   | |phi|\ (**h**)\ :sub:`2`\ :sup:`+` - |phi|\ :sub:`diff`\ :sup:`+`   |
+---------------------------------------------------------------------+---------------------------------------------------------------------+
| |phi|\ (**h**)\ :sub:`1`\ :sup:`-` + |phi|\ :sub:`diff`\ :sup:`-`   | |phi|\ (**h**)\ :sub:`2`\ :sup:`-` + |phi|\ :sub:`diff`\ :sup:`-`   |
| |phi|\ (**h**)\ :sub:`1`\ :sup:`-` - |phi|\ :sub:`diff`\ :sup:`-`   | |phi|\ (**h**)\ :sub:`2`\ :sup:`-` - |phi|\ :sub:`diff`\ :sup:`-`   |
+---------------------------------------------------------------------+---------------------------------------------------------------------+

Rewording this, the effect of anomalous scattering is to introduce a
phase shift, which is different for reflections forming a Friedel pair.
Thus there are circles with radii \|(F\ :sub:`hkl`)\ :sub:`PH`\ \| and
\|(F\ :sub:`-h-k-l`)\ :sup:`\*`\ :sub:`PH`\ \| centred at
-(**F\ :sub:`H`**\ +\ **F"\ :sub:`H`**) and
-(**F\ :sub:`H`**-**F"\ :sub:`H`**) (points B and C in the diagram),
intersecting with the circle corresponding to \|F\ :sub:`P`\ \| in only
one point (point A in the diagram).

MIRAS - Multiple Isomorphous Replacement with Anomalous Scattering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No picture available yet

This can be seen as a merging of the MIR and the SIRAS cases. There
should now be no phase ambiguity, and in fact refining the anomalous
"occupancy" starting from Aocc = 0.0 should result in these occupancies
all becoming positive, indicating the hand is correct, or all negative
indicating it is wrong.

SAD - Single Anomalous Dispersion
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For picture see `SIRAS case <#siras>`__

The SIRAS figure can be used to illustrate the case of SAD phasing
assuming F\ :sub:`H\ :sub:`real`` is zero. Here, as in the SIR phasing,
again there are only two measurements to consider: F\ :sub:`PH`\ (**h**)
and F\ :sub:`PH`\ (-**h**). The crossings will now indicate a phase of
|phi|\ :sub:`H`\ +90±\ |phi|\ :sub:`diff`, and as in the SIR case the
"best" phases will be |phi|\ (**h**)+90, and the figure of merit equals
cos\ |phi|\ :sub:`diff`. If the hand of the heavy atoms is changed, the
phase will change to -|phi|\ :sub:`H`\ +90±\ |phi|\ :sub:`diff`, the
"best" phase will be -|phi|\ (**h**)+90, and the figure of merit will be
the same. In this case the map on the other hand is actually a Babinet
or negated copy of the inverse of the true map, *i.e.*
|phi|\ :sub:`best\ :sub:`2`` = -|phi|\ :sub:`best\ :sub:`1`` + 180.

+----------------------------------------------------------------+-----------------------------------------------------------------+
| The phase ambiguities are:                                     |
+================================================================+=================================================================+
| Crossing                                                       | Hand                                                            |
+----------------------------------------------------------------+-----------------------------------------------------------------+
| |phi|\ (**h**)\ :sub:`1` + 90 + |phi|\ :sub:`diff\ :sub:`1``   | -|phi|\ (**h**)\ :sub:`1` + 90 + |phi|\ :sub:`diff\ :sub:`1``   |
| |phi|\ (**h**)\ :sub:`1` - 90 + |phi|\ :sub:`diff\ :sub:`1``   | -|phi|\ (**h**)\ :sub:`1` - 90 + |phi|\ :sub:`diff\ :sub:`1``   |
+----------------------------------------------------------------+-----------------------------------------------------------------+
| |phi|\ :sub:`best` = |phi|\ (**h**)\ :sub:`1` + 90 or          | |phi|\ :sub:`best` = -|phi|\ (**h**)\ :sub:`1` + 90 or          |
| |phi|\ (**h**)\ :sub:`1` + 90 + |phi|                          | -|phi|\ (**h**)\ :sub:`1` + 90 + |pi|                           |
+----------------------------------------------------------------+-----------------------------------------------------------------+

Maps from different hands will be enantiomorphic Babinet maps, *i.e.*
all negative density in one becomes positive density in the other, since
the |phi|\ :sub:`best`\ (-) = -|phi|\ :sub:`best`\ (+) + 180.

| 

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg10.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg8.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |SIR phase circles| image:: ../images/SIRtr.gif
   :width: 545px
   :height: 520px
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
.. |pi| image:: ../images/pitr.gif
   :width: 10px
   :height: 11px
.. |MIR phase circles| image:: ../images/MIRtr.gif
   :width: 548px
   :height: 520px
.. |SIRAS phase circles| image:: ../images/SIRADtr.gif
   :width: 545px
   :height: 516px
.. |phi| image:: ../images/pitr.gif
   :width: 8px
   :height: 16px
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
