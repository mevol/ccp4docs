SAPI (CCP4: Supported Program)
==============================

NAME
----

**sapi** - A program to find heavy atom sites.

SYNOPSIS
--------

| **sapi hklin**\ *foo\_in.mtz* **sapipks **** *foo\_sapi.pks*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

SAPI is a direct-method program to find heavy atom sites using
one-wavelength anomalous scattering data or single isomorphous
replacement data. It can also be used to solve general small molecule
structures.

SAPI is run in two passes. The `first <#first>`__ run of SAPI is used to
find list of sites, output in the **SAPIPKS** file.

| The `second <#second>`__ run is used to refine these sites and obtain
  scores using the `KAR <#kar>`__ keyword. The output is again in the
  SAPIPKS file.
| NB. currently this requires copying the contents of the **SAPIPKS**
  file in the first job to the command script of the second.
| Also, because of the way that the SAPI input works, an EOF must occur
  before the input parsing occurs. 

INPUT AND OUTPUT FILES
----------------------

The following input and output files are used by the program:

Input Files:

 HKLIN
    Input MTZ file.
    This should contain the conventional (CCP4) asymmetric unit of data
    (preferably from `TRUNCATE <./truncate.html>`__). See the
    `LABIN <#labin>`__ keyword for columns used.

Output Files:

 SAPIPKS
    Output file that contains the peak list found by the program.

KEYWORDED INPUT
---------------

(The first three letters are significant except `LABIN <#labin>`__ where
the first four are necessary)

*The first keyword must be:*

TITLE <title string>
~~~~~~~~~~~~~~~~~~~~

*The remaining keywords can be in any order. Some are compulsory (see
individual descriptions).*

The possible keywords are:

    `**CELL** <#cell>`__, `**SPG** <#spg>`__, `**CON** <#con>`__,
    `**KAR** <#kar>`__, `**LABIN** <#labin>`__, `**NRE** <#nre>`__,
    `**MAX** <#max>`__, `**LIM** <#lim>`__, `**END** <#end>`__

CELL <a> <b> <c> <alpha> <beta> <gamma>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (optional)
| Cell dimensions. If these are not given, cell dimensions will be read
  from the input MTZ file. 

SPG <space group>
~~~~~~~~~~~~~~~~~

| (optional)
| Space group (e.g. P 21 21 21); beware of the spaces in between. If
  this is not given, space group information will be read from the input
  MTZ file. 

CON <atom type> <number of atoms in unit cell >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (optional)
| Contents of unit cell. If these are not known, the program will assume
  all carbon, and estimate the number according to the cell volume.
  Note: the program is not sensitive to the atom type, i.e. you may use
  C (carbon) to model anomalous scatterers such as Se. 

KAR <atom1> <x1> <y1> <z1> [<1> <occupancy1>] ...... <atom\_n> <xn> <yn> <zn> [<n> <occupancy\_n>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (optional)
| Fractional coordinates of atom(s). The coordinates will be refined by
  the Karle recycle method. An R-factor will be calculated to assess the
  quality of the given sites and additional sites can be found. 

LABIN DF=... SIGDF=...
~~~~~~~~~~~~~~~~~~~~~~

(compulsory)

DF:

Friedel difference (OAS) or the isomorphous difference
\|F(derivative)\|-\|F(native)\| (SIR).

SIGFD:

standard deviation of DF.

NRE <number>
~~~~~~~~~~~~

| (optional)
| Number of largest E's used. 

MAX <number>
~~~~~~~~~~~~

| (optional)
| Number of trials. (default = 1000). 

LIM <rmax> <rmin>
~~~~~~~~~~~~~~~~~

Limits on data resolution in Angstrom (default: no low reso limit, 3)

END
~~~

| (compulsory)
| end of the keyword list

EXAMPLES
--------

The first run
~~~~~~~~~~~~~

-  `sapi\_run1.exam <../examples/unix/runnable/sapi_run1.exam>`__
   Example of SAPI locating Hg sites in rnase 25 data.
-  ::

       #ompdc_1.com
       sapi hklin ompdc.mtz sapipks ompdc_sapi.pks<<eof
       TITLE   ompdc one-wavelength anomalous scattering data -
       locating Se atoms
       LABIN DF=DANO2 SIGDF=SIGDANO2
       END
       eof

The second run
~~~~~~~~~~~~~~

-  `sapi\_run2.exam <../examples/unix/runnable/sapi_run1.exam>`__
   Example of SAPI refining Hg sites in rnase 25 data.
-  ::

       #ompdc_2.com
       sapi hklin ompdc.mtz sapipks ompdc_sapi.pks<<eof
       TITLE ompdc - refining the Se sites
       labin DF=DANO2 SIGDF=SIGDANO2
       KAR
             C    0.9481   0.0686    0.3858    1
             C    0.0501   0.1418    0.8754    2
             C    0.2278   0.0530    0.8609    3
             C    0.0996   0.0933    0.7587    4
             C    0.2879   0.1929    0.8885    5
             C    0.2921   0.2246    0.7965    6
             C    0.2016   0.2238    0.7753    7
             C    0.6243   0.2351    0.2025    8
             C    0.8794   0.0520    0.4618    9
             C    0.3525   0.1186    0.8852   10
             C    0.2272   0.1292    0.8899   11
             C    0.0386   0.0675    0.2715   12
             C    0.6741   0.2202    0.5788   13
             C    0.9896   0.0569    0.4514   14
             C    0.3682   0.0832    0.5957   15
             C    0.8689   0.0405    0.3536   16
             C    0.7619   0.1833    0.1968   17
             C    0.6529   0.2022    0.9082   18
       END
       eof

REFERENCES
----------

#. Q. Hao, Y. X. Gu, J. X. Yao, C. D. Zheng and H. F. Fan (2003) *J.
   Appl. Cryst.* **36** 1274-1276. "SAPI: a direct-methods program for
   finding heavy-atom sites with SAD or SIR data."

AUTHORS
-------

Y. X. Gu (1), C. D. Zheng (1) & H. F. Fan (1) & Q. Hao (1, 2)

| (1) Institute of Physics, Chinese Academy of Sciences, Beijing 100080,
  P. R. China.
| (2) MacCHESS, Wilson Synchrotron Lab, Cornell University, NY 14850,
  USA

Email: qh22@cornell.edu or fan@aphy.iphy.ac.cn
