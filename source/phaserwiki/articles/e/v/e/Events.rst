.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Events
   :name: events
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   <div id="toc" class="toc">

.. raw:: html

   <div id="toctitle">

.. rubric:: Contents
   :name: contents

.. raw:: html

   </div>

-  `1 2013 <#2013>`__
-  `2 2012 <#2012>`__
-  `3 2011 <#2011>`__
-  `4 2010 <#2010>`__
-  `5 2009 <#2009>`__
-  `6 2008 <#2008>`__
-  `7 2007 <#2007>`__
-  `8 2006 <#2006>`__
-  `9 2005 <#2005>`__
-  `10 2004 <#2004>`__
-  `11 2003 <#2003>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

New developments in Phaser are represented regularly at conferences.
Please come and talk to us at these meetings if you have any queries,
suggestions or comments.

.. rubric:: 2013
   :name: section

ECM28 Aug 25-29 2013
    Randy Read
    Airlie McCoy
    Rob Oeffner
    Andrea Thorn

CCP4 Study Weekend Jan 3-5 2013
    Randy Read
    Airlie McCoy
    Gabor Bunkoczi

.. rubric:: 2012
   :name: section-1

ECM27, 6-11 August 2012, Bergen (Norway)
    Randy Read
Gordon Conference, Diffraction Methods in Structural Biology, 15-20 July
2012, Bates College, Lewiston, ME (USA)
    Airlie McCoy
Erice Crystallography School, May 31 to June 10, 2012
    Randy Read
Computational methods for macromolecular phasing and refinement, 11
March 2012, Zaragoza (Spain)
    Gabor Bunkoczi
Phenix Workshop, 5-8 March 2012, Santa Fe, NM (USA)
    Gabor Bunkoczi
    Rob Oeffner
    Randy Read
Lunchtime bytes CCP4 Study weekend, 5-6 January 2012, Warwick (UK)
    Airlie McCoy
    Rob Oeffner

.. rubric:: 2011
   :name: section-2

Phenix Workshop, 20 September 2011, Duke (USA)
    Randy Read
    Gabor Bunkoczi
*NE-CAT workshop on low to moderate resolution phasing and refinement,
19 September, 2011, New York (USA)*
    Randy Read
XXIIth Congress of the IUCr, 22-30 August, 2011, Madrid (Spain)
    Randy Read
Phenix Netherlands/Belgium Workshop, 24 May 2011, Utrecht (The
Netherlands)
    Randy Read
Phenix Workshop, 14 March 2011, Berkeley CA (USA)
    Randy Read
    Gabor Bunkoczi
36th Lorne Conference on Protein structure and function, 6-10 February
2011, Lorne (Australia)
    *PHENIX and PHASER Workshop*
    Airlie J McCoy

.. rubric:: 2010
   :name: section-3

Phenix Workshop, 11 October 2010, Cambridge (UK)
    Airlie McCoy
    Rob Oeffner
ECM26, 29 August - 2 September 2010, Darmstadt (Germany)
    Randy Read
Gordon Conference, Diffraction Methods in Structural Biology, 18-23 July
2010, Bates College, Lewiston, ME (USA)
    *Using SAD data in Phaser*
    Randy Read
Macromolecular Crystallography Summer School and Workshop, Canadian
Light Source, 13-17 June 2010, Saskatoon (Canada)
    Randy Read
Phenix Workshop, 9 March 2010, Duke (USA)
    Randy Read
    Gabor Bunkoczi
CCP4 Study Weekend, 6-8 January 2010, Nottingham (UK)
    Randy Read

.. rubric:: 2009
   :name: section-4

X-ray methods in structural biology, 12-27 October 2009, Cold Spring
Harbour (USA)
    Randy J Read

Phenix Workshop, 3 October 2009, Berkeley, CA (USA)
    Gabor Bunkoczi

VII Regional School of Crystallography, 13-17 July 2009, Havana (Cuba)
    *Molecular replacement: Pattersons and likelihood and Experimental
    phasing*
    Gabor Bunkoczi

EMBO / MAX-INF2 Practical Course, 15-19 June 2009, Grenoble (France)
    *Density modification and Phaser for experimental phasing*
    Gabor Bunkoczi

Phenix Workshop, 12 March 2009, Durham, NC (USA)
    *Difficult MR made easy with Phaser*
    Gabor Bunkoczi

CCP4 Study Weekend, 3-4 Jan 2009, Nottingham (UK)
    *The First Map*
    Airlie J McCoy

.. rubric:: 2008
   :name: section-5

Biocrys2008, 4-11 October 2008, Oeiras (Portugal)
    *Molecular replacement: from Pattersons to likelihood*
    Gabor Bunkoczi

CCP4 Workshop, 1-5 September 2008, Tokyo (Japan)
    *Molecular replacement in Phaser and Experimental phasing in Phaser*
    Gabor Bunkoczi

CCP4 Workshop, 23-28 May 2008, Chicago, IL (USA)
    *Molecular replacement in Phaser and Experimental phasing in Phaser*
    Gabor Bunkoczi

Phenix Workshop, 26-27 March 2008, Berkeley, CA (USA)
    *Difficult MR made easy with Phaser*
    Gabor Bunkoczi

.. rubric:: 2007
   :name: section-6

MAXLAB Annual User's Meeting, 30 October - 2 November 2007, Lund
(Sweden)
    *SAD Phasing in Phenix*
    Gabor Bunkoczi

.. rubric:: 2006
   :name: section-7

Gordon Research Conference, 16-21 July 2006, Lewiston (USA)
    *Phasing with Phaser*
    Airlie J. McCoy

8th International School on the Crystallography of Biological
Macromolecules, 21-25 May 2006, Como (Italy)
    *Likelihood in Phaser*
    Randy J. Read

CCP4 Study Weekend, 6-7 Jan 2006, Leeds (UK)
    *Solving Difficult Molecular Replacement Problems with Phaser*
    Airlie J. McCoy

.. rubric:: 2005
   :name: section-8

 ACA 2005 Annual Meeting, 28 May - 2 June 2005, Orlando (USA)
    *Maximum Likelihood Molecular Replacement in Phaser*
    Airlie J. McCoy

 International School of Crystallography - 37th Course, 13 - 22 May
2005, Erice (Italy)
    *Likelihood-based molecular replacement in Phaser: view the talk*
    Randy J. Read

 International School of Crystallography - 37th Course, 13 - 22 May
2005, Erice (Italy)
    *Likelihood-based experimental phasing in Phaser: view the talk*
    Airlie J. McCoy

.. rubric:: 2004
   :name: section-9

 BioCrys2004, Fundamentals of modern methods in biocrystallography,
19-26 Nov 2004, Oeiras (Portugal)
    *Liking Likelihood*
    Airlie J. McCoy

Gordon Research Conference, 11-16 July 2004, Lewiston (USA)
    *Phasing with maximum likelihood in Phaser*
    Randy J. Read

CCP4 Study Weekend, 4-5 Jan 2004, Leeds (UK)
    *Liking Likelihood*
    Airlie J. McCoy

.. rubric:: 2003
   :name: section-10

ACA 2003 Annual Meeting, 26-31 July 2003, Covington (USA)
    Fast Molecular Replacement by Maximum Likelihood in Phaser
    Airlie J. McCoy

7th International School on Crystallography of Biological
Macromolecules, 10 - 14 May 2003, Como (Italy)
    Maximum Likelihood Molecular Replacement
    Airlie J. McCoy

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/e/v/e/Events.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Events&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Events>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section-11

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 11:37, 9 September 2013 by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__. Based on work
   by `Randy Read <../../../../articles/r/a/n/User:Randy.html>`__,
   `Gabor Bunkoczi <../../../../articles/g/a/b/User:Gaborb.html>`__ and
   `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
