.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: B-CA-mr
   :name: b-ca-mr
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. rubric:: Tutorial 1
   :name: tutorial-1

Search for 6 copies of β-Carbonic Anhydrase

+--------------------------+--------------------------+--------------------------+
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumb tleft">  |    class="thumb tleft">  |    class="thumb tleft">  |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumbinner"    |    class="thumbinner"    |    class="thumbinner"    |
|    style="width:182px;"> |    style="width:182px;"> |    style="width:182px;"> |
|                          |                          |                          |
| |image0|                 | |image2|                 | |image4|                 |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumbcaption"> |    class="thumbcaption"> |    class="thumbcaption"> |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div class="magnify"> |    <div class="magnify"> |    <div class="magnify"> |
|                          |                          |                          |
| |image1|                 | |image3|                 | |image5|                 |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| Select New Project from  | Select Set up tutorial   | Select the beta-Carbonic |
| the main menu            | data                     | Anhydrase tutorial       |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
+--------------------------+--------------------------+--------------------------+
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumb tleft">  |    class="thumb tleft">  |    class="thumb tleft">  |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumbinner"    |    class="thumbinner"    |    class="thumbinner"    |
|    style="width:182px;"> |    style="width:182px;"> |    style="width:182px;"> |
|                          |                          |                          |
| |image6|                 | |image8|                 | |image10|                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumbcaption"> |    class="thumbcaption"> |    class="thumbcaption"> |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div class="magnify"> |    <div class="magnify"> |    <div class="magnify"> |
|                          |                          |                          |
| |image7|                 | |image9|                 | |image11|                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| Open the Molecular       | Open the Phaser-MR Tab   | Load the Reflection Data |
| Replacement Tab          |                          | file                     |
|                          | .. raw:: html            |                          |
| .. raw:: html            |                          | .. raw:: html            |
|                          |    </div>                |                          |
|    </div>                |                          |    </div>                |
|                          | .. raw:: html            |                          |
| .. raw:: html            |                          | .. raw:: html            |
|                          |    </div>                |                          |
|    </div>                |                          |    </div>                |
|                          | .. raw:: html            |                          |
| .. raw:: html            |                          | .. raw:: html            |
|                          |    </div>                |                          |
|    </div>                |                          |    </div>                |
+--------------------------+--------------------------+--------------------------+
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumb tleft">  |    class="thumb tleft">  |    class="thumb tleft">  |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumbinner"    |    class="thumbinner"    |    class="thumbinner"    |
|    style="width:182px;"> |    style="width:182px;"> |    style="width:182px;"> |
|                          |                          |                          |
| |image12|                | |image14|                | |image16|                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumbcaption"> |    class="thumbcaption"> |    class="thumbcaption"> |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div class="magnify"> |    <div class="magnify"> |    <div class="magnify"> |
|                          |                          |                          |
| |image13|                | |image15|                | |image17|                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| Select the 3.5A dataset  | Select the pdb file      | Set the Variance to 61%  |
|                          |                          | id                       |
| .. raw:: html            | .. raw:: html            |                          |
|                          |                          | .. raw:: html            |
|    </div>                |    </div>                |                          |
|                          |                          |    </div>                |
| .. raw:: html            | .. raw:: html            |                          |
|                          |                          | .. raw:: html            |
|    </div>                |    </div>                |                          |
|                          |                          |    </div>                |
| .. raw:: html            | .. raw:: html            |                          |
|                          |                          | .. raw:: html            |
|    </div>                |    </div>                |                          |
|                          |                          |    </div>                |
+--------------------------+--------------------------+--------------------------+
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumb tleft">  |    class="thumb tleft">  |    class="thumb tleft">  |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumbinner"    |    class="thumbinner"    |    class="thumbinner"    |
|    style="width:182px;"> |    style="width:182px;"> |    style="width:182px;"> |
|                          |                          |                          |
| |image18|                | |image20|                | |image22|                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div                  |    <div                  |    <div                  |
|    class="thumbcaption"> |    class="thumbcaption"> |    class="thumbcaption"> |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    <div class="magnify"> |    <div class="magnify"> |    <div class="magnify"> |
|                          |                          |                          |
| |image19|                | |image21|                | |image23|                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| Select the sequence for  | Set the composition to 6 | Search for 6 copies of   |
| the composition          | copies                   | the ensemble             |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|    </div>                |    </div>                |    </div>                |
+--------------------------+--------------------------+--------------------------+
| The following            | .. raw:: html            | .. raw:: html            |
| "Additional Parameters"  |                          |                          |
| are set to reduce        |    <div                  |    <div                  |
| calculation time         |    class="thumb tleft">  |    class="thumb tleft">  |
|                          |                          |                          |
|                          | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|                          |    <div                  |    <div                  |
|                          |    class="thumbinner"    |    class="thumbinner"    |
|                          |    style="width:182px;"> |    style="width:182px;"> |
|                          |                          |                          |
|                          | |image24|                | |image26|                |
|                          |                          |                          |
|                          | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|                          |    <div                  |    <div                  |
|                          |    class="thumbcaption"> |    class="thumbcaption"> |
|                          |                          |                          |
|                          | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|                          |    <div class="magnify"> |    <div class="magnify"> |
|                          |                          |                          |
|                          | |image25|                | |image27|                |
|                          |                          |                          |
|                          | .. raw:: html            | .. raw:: html            |
|                          |                          |                          |
|                          |    </div>                |    </div>                |
|                          |                          |                          |
|                          | Set the rotation peak    | Turn off "Rescore        |
|                          | selection to 65%         | rotation" and "Rescore   |
|                          |                          | translation"             |
|                          | .. raw:: html            |                          |
|                          |                          | .. raw:: html            |
|                          |    </div>                |                          |
|                          |                          |    </div>                |
|                          | .. raw:: html            |                          |
|                          |                          | .. raw:: html            |
|                          |    </div>                |                          |
|                          |                          |    </div>                |
|                          | .. raw:: html            |                          |
|                          |                          | .. raw:: html            |
|                          |    </div>                |                          |
|                          |                          |    </div>                |
+--------------------------+--------------------------+--------------------------+
| .. raw:: html            | .. raw:: html            |                          |
|                          |                          |                          |
|    <div                  |    <div                  |                          |
|    class="thumb tleft">  |    class="thumb tleft">  |                          |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            |                          |
|                          |                          |                          |
|    <div                  |    <div                  |                          |
|    class="thumbinner"    |    class="thumbinner"    |                          |
|    style="width:182px;"> |    style="width:182px;"> |                          |
|                          |                          |                          |
| |image28|                | |image30|                |                          |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            |                          |
|                          |                          |                          |
|    <div                  |    <div                  |                          |
|    class="thumbcaption"> |    class="thumbcaption"> |                          |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            |                          |
|                          |                          |                          |
|    <div class="magnify"> |    <div class="magnify"> |                          |
|                          |                          |                          |
| |image29|                | |image31|                |                          |
|                          |                          |                          |
| .. raw:: html            | .. raw:: html            |                          |
|                          |                          |                          |
|    </div>                |    </div>                |                          |
|                          |                          |                          |
| Select "Fast" search     | Turn off the "Search     |                          |
| method                   | deeper in the rotation   |                          |
|                          | list"                    |                          |
| .. raw:: html            |                          |                          |
|                          | .. raw:: html            |                          |
|    </div>                |                          |                          |
|                          |    </div>                |                          |
| .. raw:: html            |                          |                          |
|                          | .. raw:: html            |                          |
|    </div>                |                          |                          |
|                          |    </div>                |                          |
| .. raw:: html            |                          |                          |
|                          | .. raw:: html            |                          |
|    </div>                |                          |                          |
|                          |    </div>                |                          |
+--------------------------+--------------------------+--------------------------+

Run the tutorial, and check results.

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-site" class="portlet">

.. rubric:: site
   :name: site

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Home">

   .. raw:: html

      </div>

   `Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-News">

   .. raw:: html

      </div>

   `News <../../../../articles/n/e/w/News.html>`__
-  

   .. raw:: html

      <div id="n-Events">

   .. raw:: html

      </div>

   `Events <../../../../articles/e/v/e/Events.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-documentation" class="portlet">

.. rubric:: documentation
   :name: documentation

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__
-  

   .. raw:: html

      <div id="n-Keyword-Examples">

   .. raw:: html

      </div>

   `Keyword
   Examples <../../../../articles/m/r/_/MR_using_keyword_input_9c88.html>`__
-  

   .. raw:: html

      <div id="n-Python-Examples">

   .. raw:: html

      </div>

   `Python
   Examples <../../../../articles/p/y/t/Python_Example_Scripts_7723.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Bugs">

   .. raw:: html

      </div>

   `Bugs <../../../../articles/b/u/g/Bugs.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-View-SVN">

   .. raw:: html

      </div>

   `View SVN <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-other" class="portlet">

.. rubric:: other
   :name: other

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Contact">

   .. raw:: html

      </div>

   `Contact <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developers">

   .. raw:: html

      </div>

   `Developers <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__
-  

   .. raw:: html

      <div id="n-Help">

   .. raw:: html

      </div>

   `Help <../../../../articles/c/o/n/Help%7EContents_22de.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |image0| image:: ../../../../images/thumb/e/e8/BetaCA-01.png/180px-BetaCA-01.png
   :class: thumbimage
   :width: 180px
   :height: 97px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-01.png_44e3.html
.. |image1| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-01.png_44e3.html
.. |image2| image:: ../../../../images/thumb/2/2f/BetaCA-02.png/180px-BetaCA-02.png
   :class: thumbimage
   :width: 180px
   :height: 96px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-02.png_6adb.html
.. |image3| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-02.png_6adb.html
.. |image4| image:: ../../../../images/thumb/1/13/BetaCA-03.png/180px-BetaCA-03.png
   :class: thumbimage
   :width: 180px
   :height: 95px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-03.png_9a09.html
.. |image5| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-03.png_9a09.html
.. |image6| image:: ../../../../images/thumb/b/b4/BetaCA-04.png/180px-BetaCA-04.png
   :class: thumbimage
   :width: 180px
   :height: 96px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-04.png_b419.html
.. |image7| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-04.png_b419.html
.. |image8| image:: ../../../../images/thumb/b/b5/BetaCA-05.png/180px-BetaCA-05.png
   :class: thumbimage
   :width: 180px
   :height: 96px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-05.png_ad1e.html
.. |image9| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-05.png_ad1e.html
.. |image10| image:: ../../../../images/thumb/3/3b/BetaCA-06.png/180px-BetaCA-06.png
   :class: thumbimage
   :width: 180px
   :height: 97px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-06.png_acee.html
.. |image11| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-06.png_acee.html
.. |image12| image:: ../../../../images/thumb/1/18/BetaCA-07.png/180px-BetaCA-07.png
   :class: thumbimage
   :width: 180px
   :height: 97px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-07.png_633a.html
.. |image13| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-07.png_633a.html
.. |image14| image:: ../../../../images/thumb/7/74/BetaCA-08.png/180px-BetaCA-08.png
   :class: thumbimage
   :width: 180px
   :height: 96px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-08.png_cacc.html
.. |image15| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-08.png_cacc.html
.. |image16| image:: ../../../../images/thumb/2/29/BetaCA-09.png/180px-BetaCA-09.png
   :class: thumbimage
   :width: 180px
   :height: 97px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-09.png_dff6.html
.. |image17| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-09.png_dff6.html
.. |image18| image:: ../../../../images/thumb/c/cd/BetaCA-10.png/180px-BetaCA-10.png
   :class: thumbimage
   :width: 180px
   :height: 98px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-10.png_e977.html
.. |image19| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-10.png_e977.html
.. |image20| image:: ../../../../images/thumb/0/09/BetaCA-11.png/180px-BetaCA-11.png
   :class: thumbimage
   :width: 180px
   :height: 97px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-11.png_e644.html
.. |image21| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-11.png_e644.html
.. |image22| image:: ../../../../images/thumb/5/5b/BetaCA-12.png/180px-BetaCA-12.png
   :class: thumbimage
   :width: 180px
   :height: 96px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-12.png_234d.html
.. |image23| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-12.png_234d.html
.. |image24| image:: ../../../../images/thumb/4/4a/BetaCA-13.png/180px-BetaCA-13.png
   :class: thumbimage
   :width: 180px
   :height: 98px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-13.png_208d.html
.. |image25| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-13.png_208d.html
.. |image26| image:: ../../../../images/thumb/3/3b/BetaCA-14.png/180px-BetaCA-14.png
   :class: thumbimage
   :width: 180px
   :height: 96px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-14.png_de9d.html
.. |image27| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-14.png_de9d.html
.. |image28| image:: ../../../../images/thumb/8/88/BetaCA-15.png/180px-BetaCA-15.png
   :class: thumbimage
   :width: 180px
   :height: 97px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-15.png_0654.html
.. |image29| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-15.png_0654.html
.. |image30| image:: ../../../../images/thumb/1/12/BetaCA-16.png/180px-BetaCA-16.png
   :class: thumbimage
   :width: 180px
   :height: 97px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-16.png_85ad.html
.. |image31| image:: ../../../../skins/common/images/magnify-clip.png
   :width: 15px
   :height: 11px
   :target: ../../../../articles/b/e/t/File%7EBetaCA-16.png_85ad.html
