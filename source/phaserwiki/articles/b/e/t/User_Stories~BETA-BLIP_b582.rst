.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: User Stories:BETA-BLIP
   :name: user-storiesbeta-blip
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

+-------------------+-----------------------------+-----------------------------+
| Parameter         | Value                       | Comment                     |
+===================+=============================+=============================+
| Space Group       | P3₂21                       | enantiomorphic with P3₁21   |
+-------------------+-----------------------------+-----------------------------+
| Unit Cell         | 75Å 75Å 133Å 90° 90° 120°   | average                     |
+-------------------+-----------------------------+-----------------------------+
| Resolution        | 2.5Å                        | average                     |
+-------------------+-----------------------------+-----------------------------+
| Solvent Content   | ~50%                        | average                     |
+-------------------+-----------------------------+-----------------------------+
| ASU Contents      | 1×BETA + 1×BLIP             | protein-protein complex     |
+-------------------+-----------------------------+-----------------------------+
| Model Quality     | 100% identity               | Both solved independently   |
+-------------------+-----------------------------+-----------------------------+
| Anomalous atoms   | N/A                         |                             |
+-------------------+-----------------------------+-----------------------------+
| Wavelength        | N/A                         |                             |
+-------------------+-----------------------------+-----------------------------+

Table:  Crystallographic details

Story
The case of β-lactamase (BETA)–β-lactamase inhibitor (BLIP) has been
used repeatedly as a test case for Phaser because the original structure
solution by MR using AMoRe was difficult even though good models were
available (the structures of both components had already been solved in
isolation). The difficult part of the MR solution was placing BLIP.
Phaser rapidly produces a correct solution for the complex.

This previously difficult structure solution becomes trivial because of
two algorithms implemented in Phaser.

-  The anisotropy correction; there is significant anisotropy in the
   data (the maximum B-factor difference in different directions is 32
   Å²).
-  The improved rotation-function target in MLRF, particularly in that
   the solution for BETA can be used to find the correct
   rotation-function solution for BLIP. Using the traditional Crowther
   (1972) fast rotation function, the Z score for the correct BLIP
   placement is 3.8 and the top Z score of 4.4 corresponds to an
   incorrect placement. Using MLRF and the prior knowledge about the
   placement of BETA, the correct placement of BLIP has a Z score of 6.5
   and is the highest score in the search. (These results are for data
   that have had the anisotropy correction applied, to illustrate the
   improvement given by the MLRF alone.)

| 

Keyword script(s)
::

    MODE MR_AUTO
    HKLIN beta_blip.mtz
    LABIN F=Fobs SIGF=Sigma
    ENSEMBLE BETA PDB beta.pdb ID 100
    ENSEMBLE BLIP PDB blip.pdb ID 100
    SEARCH ENSEMBLE BETA
    SEARCH ENSEMBLE BLIP

Reference(s)
    `Solving structures of protein complexes by molecular replacement
    with Phaser <http://antarctic/phaserwiki/images/4/40/Ba5095.pdf>`__
    McCoy AJ
    Acta Cryst. (2007). D63, 32-41

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
