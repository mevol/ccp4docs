|previous listing| | | |plots listing| | |\ *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Plot 10. Distorted geometry plots
=================================

|image3|

Description
-----------

These plots shows all distorted main-chain bond lengths, main-chain bond
angles, and planar groups.

The parameters defining how distorted these properties need to be before
being plotted here are given in the
`procheck.prm <../parameters/manopt_10.html>`__ parameter file.

For each main-chain bond length and angle plotted, the plot shows the
**idea**\ l value (as defined by the **`Engh &
Huber <../manrefs.html#ENGH>`__** small-molecule data), the actual
value, and the difference between the two.

For each distorted planar groups, three orthogonal projections are
plotted and the value shown is the **RMS** distance of the atoms from
the best-fit plane.

Options
-------

The main options for the plot are:-

-  Cut-off deviation from **ideal** bond length (Å).
-  Cut-off deviation from **ideal** bond angle (degrees).
-  Cut-off RMS distances from planarity.
-  The plot can be in colour or black-and-white.

These options can be altered by editing the parameter file,
**procheck.prm**, as described `here <../parameters/manopt_10.html>`__.

--------------

|previous listing| | | |plots listing| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous listing| image:: ../leftr.gif
   :target: plot_09.html
.. | | image:: ../12p.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |image3| image:: plot_10.gif
   :width: 306px
   :height: 434px
   :target: plot_10.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
