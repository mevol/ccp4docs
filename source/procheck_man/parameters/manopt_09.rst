|previous page| | | |plot parameters| | | |next page| | |\ *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

9. RMS distances from planarity
===============================

There are **8** parameters defining the `**RMS distances from
planarity** <../examples/plot_09.html>`__ plots, as follows:-

--------------

::

    9. RMS distances from planarity
    -------------------------------
    Y     <- Background shading on graphs (Y/N)?
    0.03  <- RMS distance for highlighting for ring groups 
    0.02  <-  "      "     "       "        "  other groups 
    N     <- Produce a COLOUR PostScript file (Y/N)?
    WHITE        <- Background colour of page
    CREAM        <- Background shading on graphs
    PURPLE       <- Colour of histogram bars
    RED          <- Colour of highlighted histogram bars

--------------

Description of options:-
------------------------

-  **Background shading** - This option determines whether the graph
   background of each plot is to be lightly shaded when the plot is in
   black-and-white.
-  **RMS distance for highlighting** - These two options determine which
   histogram bars in the plots are to be highlighted. Bars corresponding
   to RMS distances from planarity greater than the values specified
   here will be highlighted. The two different values apply separately
   to ring groups (Phe, Tyr, Trp and His) and to planar end-groups (Arg,
   Asn, Asp, Gln and Glu).
-  **Produce a COLOUR PostScript file** - This option defines whether a
   colour or black-and-white plot is required. Note that if the `Colour
   all plots <manopt_0a.html>`__ option is set to **Y**, a colour
   PostScript file will be produced irrespective of the setting here.

   The colour definitions on the following lines use the \`names' of
   each colour as defined in the colour table at the bottom of the
   parameter file (see `Colours <manopt_cols.html>`__). If the name of a
   colour is mis-spelt, or does not appear in the colour table, then
   **white** will be taken instead. Each colour can be altered to suit
   your taste and aesthetic judgement, as described in the
   `Colours <manopt_cols.html>`__ section.

-  **Background colour** - This option defines the background colour of
   the page on which the plots are drawn.
-  **Colours** - The various additional colours on the lines that follow
   define the colours for the highlighted and unhighlighted histogram
   bars, and the background colour of each graph.

--------------

|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_08.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_10.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_10.html
