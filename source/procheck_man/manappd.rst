|previous section| | | |contents page| | | |next appendix| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Appendix D - Residue-by-residue listing
=======================================

An example of a residue-by-residue listing is given
`here <examples/print.out>`__. The different parts of it are described
in detail below. Some of the key figures are shown on a separate
`summary page <examples/1abc.sum>`__.

Part 1. Residue information
---------------------------

The first part of the residue-by-residue listing (the **.out** file)
deals with a number of stereochemical parameters, as will be described
below.

**`Explanatory notes. <examples/print_0.out>`__** The first page gives
some explanatory notes about the stereochemical parameters used. These
notes include the "ideal" values, and corresponding standard deviations,
against which the values calculated for your structure are compared. The
"ideals" used here come from an analysis of 118 high-resolution
structures performed by `Morris *et al.*
(1992) <manrefs.html#MORRIS>`__, and are listed in Table 1 of `Appendix
A <manappa.html>`__.

Note that, the printing of this explanatory text can be suppressed, if
required, by amending the parameter file **procheck.prm** (see
`Customizing the PROCHECK plots <man5.html>`__).

**`Residue-by-residue information. <examples/print_1.out>`__** The
explanatory text is followed by an analysis of each of the
stereochemical parameters for each residue in the structure. Each value
is highlighted by asterisks and plus-signs if it deviates from the
"ideal" by more than 1 standard deviation. An **asterisk** represents
**one** standard deviation, and a **plus-sign** represents **half** a
standard deviation. So, a highlight such as **+\*\*\*** indicates that
the value of the parameter is between **3.5** and **4.0** standard
deviations from the ideal. Where the deviation is more than **4.5**
standard deviations, its numerical value is shown instead: for example,
**\*5.5\*** represents **5.5** standard deviations.

The appearance of the listing can be altered to some extent by editing
the parameter file **procheck.prm** (see `Customizing the PROCHECK
plots <man5.html>`__). This allows you to show, say, only the asterisks
and not have the values themselves printed. You can also include only
those values that are more than a given number of standard deviations
from the ideal.

The information shown for each residue is as follows:

 1. **Residue number**
    as given in the original coordinates file.
 2. **Chain identifier**
    where relevant, picked up from the original coordinates file.
 3. **Sequential number**
    starting at 1 for the first residue and numbering the residues
    sequentially from then on. This may differ from the residue
    numbering given in the original coordinates file.
 4. **Kabsch & Sander secondary structure assignment**
    assignment of secondary structure according to the method of `Kabsch
    & Sander (1983) <manrefs.html#KABSCH>`__. The codes used are as
    follows:

    ::

            B - residue in isolated beta-bridge    S - bend                   
            E - extended strand, participates      T - hydrogen-bonded turn   
                in beta-ladder                     e - extension of beta-strand
            G - 3-helix (3/10 helix)               g - extension of 3/10 helix
            H - 4-helix (alpha-helix)              h - extension of alpha-helix
            I - 5-helix (pi-helix)             

    The lower-case assignments are our extensions of the Kabsch & Sander
    definition and are obtained by slightly relaxing their criteria.

 5. **Region of Ramachandran plot**
    a single letter code identifies which region of the Ramachandran
    plot the residue is in. For end residues and glycines this
    assignment does not apply, so is shown by a hyphen, \`-'. The other
    codes are as follows:

    ::

             A - Core alpha          L - Core left-handed alpha  
             a - Allowed alpha       l - Allowed left-handed alpha
            ~a - Generous alpha     ~l - Generous left-handed alpha
             B - Core beta           p - Allowed epsilon         
             b - Allowed beta       ~p - Generous epsilon        
            ~b - Generous beta      XX - Outside major areas     
                                         (ie disallowed)

 6. **Chi-1 dihedral angle**
    three separate columns are given for the three possible
    conformations of chi-1: gauche minus, trans, and gauche plus.
 7. **Chi-2 dihedral angle**
    only the values for the chi-2 dihedral angles in the **trans**
    conformation are shown.
 8. **Proline phi**
    the phi torsion angle for proline residues only.
 9. **Phi helix**
    the phi torsion angle for all residues identified as being in an
    alpha-helix by the H of the Kabsch & Sander secondary structure
    assignment code.
 10. **Helix psi**
    as above, but for the psi torsion angle.
 11. **Chi-3 dihedral angle**
    being the torsion angle defined by the S-S bridge in a disulphide
    bond, with separate columns for the right- and left-handed
    conformations.
 12. **Disulph bond**
    sulphur-sulphur distance, in A, between paired cysteine residues.
 13. **H-bond en.**
    estimated strength of the main-chain hydrogen bond (in kcal/mol),
    where applicable, calculated using the method of `Kabsch & Sander
    (1983) <manrefs.html#KABSCH>`__.
 14. **Chirality C-alpha**
    value of the zeta "virtual" torsion angle, defined by the atoms
    Calpha, N, C, and Cbeta. This is a "virtual" torsion angle as it is
    not defined along an actual bond.
 15. **Bad contacts**
    number of bad contacts for this residue, as defined by non-bonded
    atoms at a distance of <= 2.6A. The bad contacts are listed at the
    end of the print-out (see Part 3 below).
 16. **Max dev**
    this shows the maximum deviation (in terms of asterisks, etc) of all
    the columns in the current row.

At the end of this print-out, the column totals show the maximum
deviation in each column, the column's mean value and standard
deviation, and number of values it contains. If the mean values
themselves deviate significantly from the "ideals", they too are
highlighted by asterisks.

Part 2. Main-chain bond lengths and bond angles
-----------------------------------------------

The `second part of the listing <examples/print_2.out>`__ analyses the
main-chain bond lengths and bond angles of your protein structure. As
before, any deviations in the actual bond-lengths and bond angles from
the "ideal" values are highlighted with asterisks and plus signs.

At the end of this print-out, the different bond lengths and bond angles
are summarised in two tables giving the minimum, maximum, and mean
values of each type, together with their standard deviations.

The "ideal" values used are given at the head of the listing (though the
printing of these can be suppressed by amending the parameter file
procheck.prm). The ideals are as determined from the analysis of
small-molecule data by `Engh & Huber (1991) <manrefs.html#ENGH>`__ and
are shown in Table 2 of `Appendix A <manappa.html#TABLE_A.2>`__.

Part 3. Bad contacts listing
----------------------------

The `bad contacts listing <examples/print_3.out>`__ shows the atom-pairs
involved, the type of contact, and the separation between the two atoms.
As already mentioned, bad contacts are defined here as any pair of
non-bonded atoms that are at a distance of **<=2.6Å** from one another.

Part 4. Summary statistics and quality assessment
-------------------------------------------------

The `final part of the print-out <examples/print_4.out>`__ reproduces
the statistics printed on Plots 1, 4 and 5 (`Sample
Plots <examples/index.html>`__). It also gives an overall assessment of
the structure's quality using the `Morris *et al.*
(1992) <manrefs.html#MORRIS>`__ stereochemical classification scheme.
Here a number from **1** to **4** is assigned to the structure for each
of three separate stereochemical parameters (**1** being the best and
**4** the worst score). Finally, it prints an analysis of the various
overall *G*-factors calculated for the structure. Any *G*-factors below
**-1.0** may indicate properties that need to be investigated more
closely.

--------------

|previous section| | | |contents page| | | |next appendix| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous section| image:: leftb.gif
   :target: manappc.html
.. | | image:: 12p.gif
.. |contents page| image:: uupb.gif
   :target: index.html
.. |next appendix| image:: rightb.gif
   :target: manappe.html
