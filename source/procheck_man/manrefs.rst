|contents page| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

REFERENCES
==========

Adobe Systems Inc. (1985). PostScript Language Reference Manual.
Addison-Wesley, Reading, MA.

Allen F H, Bellard S, Brice M D, Cartwright B A, Doubleday A, Higgs H,
Hummelink T, Hummelink-Peters B G, Kennard O, Motherwell W D S, Rodgers
J R & Watson D G (1979). The Cambridge Crystallographic Data Centre:
computer-based search, retrieval, analysis and display of information.
*Acta Cryst*., **B35**, 2331--2339.

Bernstein F C, Koetzle T F, Williams G J B, Meyer E F Jr, Brice M D,
Rogers J R, Kennard O, Shimanouchi T & Tasumi M (1977). The Protein Data
Bank: a computer-based archival file for macromolecular structures. *J.
Mol. Biol.*, **112**, 535-542.

Engh R A & Huber R (1991). Accurate bond and angle parameters for X-ray
protein structure refinement. *Acta Cryst.*, **A47**, 392-400.

IUPAC-IUB Commission on Biochemical Nomenclature (1970). Abbreviations
and symbols for the description of the conformation of polypeptide
chains. *J. Mol. Biol.*, **52**, 1-17.

Kabsch W & Sander C (1983). Dictionary of protein secondary structure:
pattern recognition of hydrogen-bonded and geometrical features.
*Biopolymers*, **22**, 2577-2637.

Laskowski R A, MacArthur M W, Moss D S & Thornton J M
(1993).\ `PROCHECK: a program to check the stereochemical quality of
protein structures <paper1.html>`__. *J. Appl. Cryst.*, **26**, 283-291.

Morris A L, MacArthur M W, Hutchinson E G & Thornton J M
(1992).\ `Stereochemical quality of protein structure
coordinates <paper2.html>`__. *Proteins*, **12**, 345-364.

Nishikawa K & Ooi T (1986). Radial locations of amino-acid residues in a
globular protein - correlation with the sequence. *J. Biochem.*,
**100**, 1043-1047.

--------------

|contents page| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |contents page| image:: uupb.gif
   :target: index.html
.. | | image:: 12p.gif

